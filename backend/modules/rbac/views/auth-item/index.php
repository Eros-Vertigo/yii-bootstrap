<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\rbac\models\search\AuthItem */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Auth Items';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-item-index card shadow p-4">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Auth Item', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

        <?php Pjax::begin(); ?>
                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
            <?= GridView::widget([
            'options'       => ['class' => 'table-responsive card-body'],
            'tableOptions'  => ['class' => 'table'],
            'dataProvider' => $dataProvider,
            'columns'       => [
                ['class' => 'yii\grid\SerialColumn'],
                'name',
                'type',
                'description:ntext',
                'rule_name',
                'data',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttonOptions' => ['class' => 'table-action'],
                    'icons' => [
                        'eye-open' => Html::tag('img', '', ['src' => '/icons/eye.svg']),
                        'pencil' => Html::tag('img', '', ['src' => '/icons/pencil-square.svg']),
                        'trash' => Html::tag('img', '', ['src' => '/icons/trash.svg'])
                    ]
                ],
            ],
        ]); ?>
    
        <?php Pjax::end(); ?>

</div>
