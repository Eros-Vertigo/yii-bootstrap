<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\rbac\models\search\AuthAssignmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Auth Assignments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-assignment-index card shadow p-4">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Auth Assignment', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?= $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'options'       => ['class' => 'table-responsive card-body'],
            'tableOptions'  => ['class' => 'table'],
            'dataProvider'  => $dataProvider,
            'columns'       => [
                ['class' => 'yii\grid\SerialColumn'],

                'item_name',
                'user_id',
                'created_at',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update} {delete}',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
//                            return
                        }
                    ]
                ],
            ],
        ]); ?>
    <?php Pjax::end(); ?>

</div>
