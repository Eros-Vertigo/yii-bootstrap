<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\rbac\models\search\AuthAssignmentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auth-assignment-search card-header bg-white">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1,
            'class' => 'row row-cols-md-auto align-items-center'
        ],
        'fieldConfig' => [
            'template'      => "{label} :\n{input}\n{error}",
            'inputOptions'  => ['class' => 'form-control form-control-sm'],
        ],
    ]); ?>

    <?= $form->field($model, 'item_name') ?>

    <?= $form->field($model, 'user_id') ?>

    <div class="col-12 pt-2">
        <?= Html::submitButton('搜索', ['class' => 'btn btn-outline-primary btn-sm mb-2']) ?>
        <?= Html::a('添加', \yii\helpers\Url::to('create'), ['class' => 'btn btn-outline-success btn-sm mb-2']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
