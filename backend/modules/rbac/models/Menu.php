<?php

namespace backend\modules\rbac\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%menu}}".
 *
 * @property int $id
 * @property string|null $name 菜单名称
 * @property int|null $parent 父级
 * @property string|null $route 路由
 * @property int|null $order 排序
 * @property string|null $icon 图标
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%menu}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent', 'order'], 'integer'],
            [['name'], 'string', 'max' => 32],
            [['route'], 'string', 'max' => 255],
            [['icon'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => '菜单名称',
            'parent' => '父级',
            'route' => '路由',
            'order' => '排序',
            'icon' => '图标',
        ];
    }

    public static function parentMap(): array
    {
        $temp = self::find()->where(['parent' => null])->asArray()->all();

        return ArrayHelper::map($temp, 'id', 'name');
    }
}
