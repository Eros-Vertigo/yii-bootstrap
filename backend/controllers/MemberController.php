<?php

namespace backend\controllers;

use backend\models\Member;
    use backend\models\search\MemberSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
* MemberController implements the CRUD actions for Member model.
*/
class MemberController extends Controller
{
/**
* @inheritDoc
*/
public function behaviors()
{
return array_merge(
parent::behaviors(),
[
'verbs' => [
'class' => VerbFilter::className(),
'actions' => [
'delete' => ['POST'],
],
],
]
);
}

/**
* Lists all Member models.
* @return mixed
*/
public function actionIndex()
{
    $searchModel = new MemberSearch();
    $dataProvider = $searchModel->search($this->request->queryParams);

    return $this->render('index', [
    'searchModel' => $searchModel,
    'dataProvider' => $dataProvider,
    ]);
}

/**
* Displays a single Member model.
* @param int $user_id User ID
* @return mixed
* @throws NotFoundHttpException if the model cannot be found
*/
public function actionView($user_id)
{
return $this->render('view', [
'model' => $this->findModel($user_id),
]);
}

/**
* Creates a new Member model.
* If creation is successful, the browser will be redirected to the 'view' page.
* @return mixed
*/
public function actionCreate()
{
$model = new Member();

if ($this->request->isPost) {
if ($model->load($this->request->post()) && $model->save()) {
return $this->redirect(['view', 'user_id' => $model->user_id]);
}
} else {
$model->loadDefaultValues();
}

return $this->render('create', [
'model' => $model,
]);
}

/**
* Updates an existing Member model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param int $user_id User ID
* @return mixed
* @throws NotFoundHttpException if the model cannot be found
*/
public function actionUpdate($user_id)
{
$model = $this->findModel($user_id);

if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
return $this->redirect(['view', 'user_id' => $model->user_id]);
}

return $this->render('update', [
'model' => $model,
]);
}

/**
* Deletes an existing Member model.
* If deletion is successful, the browser will be redirected to the 'index' page.
* @param int $user_id User ID
* @return mixed
* @throws NotFoundHttpException if the model cannot be found
*/
public function actionDelete($user_id)
{
$this->findModel($user_id)->delete();

return $this->redirect(['index']);
}

/**
* Finds the Member model based on its primary key value.
* If the model is not found, a 404 HTTP exception will be thrown.
* @param int $user_id User ID
* @return Member the loaded model
* @throws NotFoundHttpException if the model cannot be found
*/
protected function findModel($user_id)
{
if (($model = Member::findOne($id)) !== null) {
return $model;
}

throw new NotFoundHttpException('The requested page does not exist.');
}
}
