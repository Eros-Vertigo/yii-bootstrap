<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'EzD4L3WzjGPMBooC46EQRj3LFpgBNGnc',
        ],
    ],
];

/**
 * @param array $config
 * @return array
 */
function extracted(array $config): array
{
    if (!YII_ENV_TEST) {
        // configuration adjustments for 'dev' environment
        $config['bootstrap'][] = 'debug';
        $config['modules']['debug'] = [
            'class' => 'yii\debug\Module',
        ];

        $config['bootstrap'][] = 'gii';
        $config['modules']['gii'] = [
            'class' => 'yii\gii\Module',
            'allowedIPs' => ['*'],
            'generators' => [
                'crud' => [
                    'class' => 'yii\gii\generators\crud\Generator',
                    'templates' => [
                        'layuiCrud' => '@common/gii/crud'
                    ]
                ],
                'model' => [
                    'class' => 'yii\gii\generators\model\Generator',
                    'templates' => [
                        'layuiModel' => '@common/gii/model',
                        'apiModel'   => '@common/gii/apiModel'
                    ]
                ],
                'controller' => [
                    'class' => 'yii\gii\generators\controller\Generator',
                    'templates' => [
                        'apiController' => '@common/gii/apiController'
                    ]
                ],
            ]
        ];
    }

    return $config;
}

return extracted($config);
