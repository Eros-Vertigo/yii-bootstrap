<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%config_openapi}}".
 *
 * @property int $id
 * @property string $key
 * @property string|null $value
 * @property string|null $secret
 * @property string|null $remark
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class ConfigOpenapi extends \yii\db\ActiveRecord
{
    public function behaviors(): array
    {
        return [
            'class' => TimestampBehavior::class
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%config_openapi}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['key'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['key', 'value', 'secret'], 'string', 'max' => 255],
            [['remark'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'key' => '键名',
            'value' => '键值',
            'secret' => '密钥',
            'remark' => '备注',
            'created_at' => '创建时间',
            'updated_at' => '更新时间',
        ];
    }
}
