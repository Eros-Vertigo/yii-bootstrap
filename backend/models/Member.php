<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%member}}".
 *
 * @property int $user_id
 * @property string|null $username
 * @property string|null $nickname
 * @property string|null $mobile
 * @property string|null $password
 * @property string|null $avatar
 * @property string|null $province
 * @property string|null $city
 * @property string|null $region
 * @property float|null $balance
 * @property string|null $salt
 * @property int|null $fans_num
 * @property int|null $focs_num
 * @property int|null $like_num
 * @property int|null $del_flg
 * @property int|null $vip_flg
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class Member extends \yii\db\ActiveRecord
{
    public function behaviors(): array
    {
        return [
            'class' => TimestampBehavior::class
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%member}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['balance'], 'number'],
            [['fans_num', 'focs_num', 'like_num', 'del_flg', 'vip_flg', 'created_at', 'updated_at'], 'integer'],
            [['username', 'nickname'], 'string', 'max' => 32],
            [['mobile'], 'string', 'max' => 11],
            [['password', 'avatar', 'province', 'city', 'region'], 'string', 'max' => 255],
            [['salt'], 'string', 'max' => 8],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'user_id' => 'User ID',
            'username' => '用户名',
            'nickname' => '昵称',
            'mobile' => '手机号',
            'password' => '密码',
            'avatar' => '头像',
            'province' => 'Province',
            'city' => 'City',
            'region' => 'Region',
            'balance' => '余额',
            'salt' => '盐值｜邀请码',
            'fans_num' => '粉丝数',
            'focs_num' => '关注数',
            'like_num' => '点赞数',
            'del_flg' => '删除',
            'vip_flg' => 'vip',
            'created_at' => '创建时间',
            'updated_at' => '更新时间',
        ];
    }
}
