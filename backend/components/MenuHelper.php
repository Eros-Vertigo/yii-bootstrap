<?php
namespace backend\components;

use backend\models\Menu;
use Yii;
use yii\base\Widget;
use yii\bootstrap4\Html;

class MenuHelper extends Widget
{
    public array $menu;

    public function init()
    {
        $this->menu = Menu::find()->where(['parent' => NULL])->orderBy('order desc')->all();
    }

    public function run(): string
    {
        return $this->disposeMenu();
    }

    private function disposeMenu(): string
    {
        $temp = Html::beginTag('ul', ['class' => 'nav flex-column']);

        array_walk($this->menu, function ($item) use (&$temp) {
            $temp .= $this->createParent($item);
        });

        $temp .= Html::endTag('ul');
        return $temp;
    }

    private function createParent($item): string
    {
        $temp = Html::beginTag('li', ['class' => 'sidebar-item']);
        if (!empty($item->children)) {
            $temp .= Html::a(Html::tag('i', '', ['class' => $item->icon.' mr-2']).$item->name, "#collapse".$item->id, [
                'class'         => 'sidebar-link collapsed',
                'data-bs-toggle'   => "collapse",
                'role'          => "button",
                'aria-expanded' => "false",
                'aria-controls' => "collapse".$item->id
            ]);
            $temp .= $this->createChildren($item);
        } else {
            $temp .= Html::a(Html::tag('i', '', ['class' => $item->icon.' mr-2']).$item->name, $item->route, ['class' => 'sidebar-link']);
        }
        $temp .= Html::endTag('li');
        return $temp;
    }

    private function createChildren($parent): string
    {
        $temp = Html::beginTag('ul', ['class' => 'collapse', 'id' => 'collapse'.$parent->id]);
        array_walk($parent->children, function ($item) use (&$temp) {
            $temp .= $this->createParent($item);
        });
        $temp .= Html::endTag('ul');
        return $temp;
    }
}