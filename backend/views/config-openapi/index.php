<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ConfigOpenapiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Config Openapis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="config-openapi-index card shadow p-4">

        <?php Pjax::begin(); ?>
                <?=  $this->render('_search', ['model' => $searchModel]); ?>
    
            <?= GridView::widget([
        'options'       => ['class' => 'table-responsive card-body'],
        'tableOptions'  => ['class' => 'table'],
        'dataProvider' => $dataProvider,
        'columns'       => [
            ['class' => 'yii\grid\SerialColumn'],

                    'id',
            'key',
            'value',
            'secret',
            'remark',
            //'created_at',
            //'updated_at',

        [
        'class' => 'yii\grid\ActionColumn',
        'buttonOptions' => ['class' => 'table-action'],
        'icons' => [
        'eye-open' => Html::tag('img', '', ['src' => '/icons/eye.svg']),
        'pencil' => Html::tag('img', '', ['src' => '/icons/pencil-square.svg']),
        'trash' => Html::tag('img', '', ['src' => '/icons/trash.svg'])
        ]
        ],
        ],
        ]); ?>
    
        <?php Pjax::end(); ?>

</div>
