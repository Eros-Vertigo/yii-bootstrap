<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\search\ConfigOpenapiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="config-openapi-search card-header bg-white">

    <?php $form = ActiveForm::begin([
        'action'    => ['index'],
        'method'    => 'get',
        'options'   => [
            'data-pjax' => 1,
            'class' => 'row row-cols-md-auto align-items-center'
        ],
        'fieldConfig' => [
            'template'      => "{label} :\n{input}\n{error}",
            'inputOptions'  => ['class' => 'form-control form-control-sm'],
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'key') ?>

    <?= $form->field($model, 'value') ?>

    <?= $form->field($model, 'secret') ?>

    <?= $form->field($model, 'remark') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="col-12 pt-2">
        <?= Html::submitButton('搜索', ['class' => 'btn btn-outline-primary btn-sm']) ?>
        <?= Html::a('添加', Url::to(['create']), ['class' => 'btn btn-outline-success btn-sm']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
