<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\ConfigOpenapi */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Config Openapis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="config-openapi-view card shadow p-4">

    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
                'id',
            'key',
            'value',
            'secret',
            'remark',
            'created_at',
            'updated_at',
    ],
    ]) ?>

</div>
