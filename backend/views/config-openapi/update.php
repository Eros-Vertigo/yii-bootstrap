<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ConfigOpenapi */

$this->title = 'Update Config Openapi: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Config Openapis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="config-openapi-update">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
