<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ConfigOpenapi */

$this->title = 'Create Config Openapi';
$this->params['breadcrumbs'][] = ['label' => 'Config Openapis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="config-openapi-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
