<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Member */

$this->title = $model->user_id;
$this->params['breadcrumbs'][] = ['label' => 'Members', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="member-view card shadow p-4">

    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
                'user_id',
            'username',
            'nickname',
            'mobile',
            'password',
            'avatar',
            'province',
            'city',
            'region',
            'balance',
            'salt',
            'fans_num',
            'focs_num',
            'like_num',
            'del_flg',
            'vip_flg',
            'created_at',
            'updated_at',
    ],
    ]) ?>

</div>
