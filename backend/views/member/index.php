<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\MemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Members';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="member-index card shadow p-4">

    <?php Pjax::begin(); ?>
    <?= $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'options' => ['class' => 'table-responsive card-body'],
        'tableOptions' => ['class' => 'table'],
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'user_id',
            'username',
            'nickname',
            'mobile',
            'password',
            //'avatar',
            //'province',
            //'city',
            //'region',
            //'balance',
            //'salt',
            //'fans_num',
            //'focs_num',
            //'like_num',
            //'del_flg',
            //'vip_flg',
            //'created_at',
            //'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'buttonOptions' => ['class' => 'table-action'],
                'icons' => [
                    'eye-open' => Html::tag('img', '', ['src' => '/icons/eye.svg']),
                    'pencil' => Html::tag('img', '', ['src' => '/icons/pencil-square.svg']),
                    'trash' => Html::tag('img', '', ['src' => '/icons/trash.svg'])
                ]
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
