<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\search\MemberSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="member-search card-header bg-white">

    <?php $form = ActiveForm::begin([
        'action'    => ['index'],
        'method'    => 'get',
        'options'   => [
            'data-pjax' => 1,
            'class' => 'row row-cols-md-auto align-items-center'
        ],
        'fieldConfig' => [
            'template'      => "{label} :\n{input}\n{error}",
            'inputOptions'  => ['class' => 'form-control form-control-sm'],
        ],
    ]); ?>

    <?= $form->field($model, 'username') ?>

    <?= $form->field($model, 'nickname') ?>

    <?= $form->field($model, 'mobile') ?>

    <div class="col-12 pt-2">
        <?= Html::submitButton('搜索', ['class' => 'btn btn-outline-primary btn-sm']) ?>
        <?= Html::a('添加', 'create', ['class' => 'btn btn-outline-success btn-sm']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
