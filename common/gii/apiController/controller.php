<?php
/**
 * This is the template for generating a controller class file.
 */
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\controller\Generator */

echo "<?php\n";
?>
namespace <?= $generator->getControllerNamespace() ?>;

use frontend\controllers\RestFulController;
use yii\data\ActiveDataProvider;

class <?= StringHelper::basename($generator->controllerClass) ?> extends RestFulController
{
<?php foreach ($generator->getActionIDs() as $action): ?>
    //模型
    //public $modelClass = 'frontend\modules\v1\models\';
    //格式化
    public $serializer = [
        'class'              => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    //行为
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['except'] = ['index'];
        return $behaviors;
    }

    //动作
    public function actions(): array
    {
        $actions = parent::actions();
        // 使用 "prepareDataProvider()" 方法自定义数据 provider 
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        return $actions;
    }

    /**
     * 
     * @return [type] [description]
     */
    public function prepareDataProvider()
    {
        $model = new $this->modelClass;

        return new ActiveDataProvider([
            'query' => $model->find()->where(['del_flg' => 0])
        ]);
    }
<?php endforeach; ?>
}
