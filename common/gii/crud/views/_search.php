<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

echo "<?php\n";
?>

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->searchModelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-search card-header bg-white">

    <?= "<?php " ?>$form = ActiveForm::begin([
        'action'    => ['index'],
        'method'    => 'get',
        'options'   => [
            'data-pjax' => 1,
            'class' => 'row row-cols-md-auto align-items-center'
        ],
        'fieldConfig' => [
            'template'      => "{label} :\n{input}\n{error}",
            'inputOptions'  => ['class' => 'form-control form-control-sm'],
        ],
    ]); ?>

<?php
    $count = 0;
    foreach ($generator->getColumnNames() as $attribute) {
        if (++$count < 6) {
            echo "    <?= " . $generator->generateActiveSearchField($attribute) . " ?>\n\n";
        } else {
            echo "    <?php // echo " . $generator->generateActiveSearchField($attribute) . " ?>\n\n";
        }
    }
?>
    <div class="col-12 pt-2">
        <?= "<?= " ?>Html::submitButton(<?= $generator->generateString('搜索') ?>, ['class' => 'btn btn-outline-primary btn-sm']) ?>
        <?= "<?= " ?>Html::a(<?= $generator->generateString('添加') ?>, Url::to(['create']), ['class' => 'btn btn-outline-success btn-sm']) ?>
    </div>

    <?= "<?php " ?>ActiveForm::end(); ?>

</div>
