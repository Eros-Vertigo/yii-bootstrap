<?php

namespace common\components;

use common\models\UploadResource;
use Exception;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;
use frontend\components\Alibaba;
use Yii;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;
use yii\web\UploadedFile;

/**
 * 上传文件
 * @author 小原
 */
class UploadCommon extends BaseObject
{
    public object $file;
    public string $upload_type;
    public int $oss_flg;
    public string $oss_url;
    private string $path;
    private string $extension;
    private string $filename;
    private string $cover;

    public function init()
    {
        $this->path = Yii::getAlias('@uploads');
        $this->oss_url = "";
        $this->cover = "";
    }

    public function upload()
    {
        $data = [];
        $this->file = UploadedFile::getInstanceByName('file');
        try {
            if ($this->file->error != 0) {
                throw new Exception($this->file->getHasError(), 400);
            }

            $this->extension = $this->file->getExtension();
            $this->getFileName();

            if (array_search($this->extension, Yii::$app->params['upload_type'][$this->upload_type]) === false) {
                throw new Exception("上传格式错误,请检查上传文件格式", 400);
            }

            $result = $this->file->saveAs($this->getPath() . '.' . $this->extension);
            if (!$result) {
                throw new Exception("上传失败", 400);
            }
            if (in_array($this->extension, Yii::$app->params['upload_type']['video'])) {
                $this->extractCover();
            }
            $this->getOssUrl();

            $data = UploadResource::saveResource([
                'url' => RESOURCE_URL . $this->extension . '/' . date("Ymd") . '/' . $this->filename . '.' . $this->extension,
                'oss_url' => $this->oss_url,
                'dirname' => $this->getPath() . '.' . $this->extension,
                'cover' => $this->cover,
                'size' => $this->file->size,
                'mime' => $this->file->type,
                'type' => $this->upload_type
            ]);

            if (!$data) {
                throw new Exception("保存失败", 400);
            }
        } catch (Exception $e) {
            $data = ['msg' => $e->getMessage()];
        }
        return $data;
    }

    /**
     * 文件名
     */
    public function getFileName()
    {
        $this->filename = date("Ymd") . substr(time(), 0, 4) . mt_rand(1000, 9999);
    }

    /**
     * 文件路径
     * @return string
     */
    public function getPath(): string
    {
        $dir = $this->path . '/' . $this->extension . '/' . date("Ymd") . '/';
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        return $dir . $this->filename;
    }

    /**
     * 提取封面
     */
    private function extractCover()
    {
        if (YII_ENV == 'dev') {
            $ffmpeg = FFMpeg::create([
                'ffmpeg.binaries' => '/usr/bin/ffmpeg',
                'ffprobe.binaries' => '/usr/bin/ffprobe',
            ]);
        } else {
            $ffmpeg = FFMpeg::create([
                'ffmpeg.binaries' => '/usr/local/bin/ffmpeg',
                'ffprobe.binaries' => '/usr/local/bin/ffprobe',
            ]);
        }
        $video = $ffmpeg->open($this->getPath() .'.'. $this->extension);

        $frame = $video->frame(TimeCode::fromSeconds(2));//提取第几秒的图像
        $frame->save($this->getPath() . '_cover.jpg');

        $this->cover = $this->getPath() . '_cover.jpg';
    }

    /**
     * 上传oss
     * @throws InvalidConfigException
     */
    private function getOssUrl()
    {
        if ($this->oss_flg) {
            $alibaba = Yii::createObject([
                'class' => Alibaba::class,
                'bucket' => 'zyrkeji',
                'object' => 'short-video/' . date("Ymd") . '/' . $this->filename . '.' . $this->extension,
                'filePath' => $this->getPath() . '.' . $this->extension
            ]);
            $temp = $alibaba->ossUploadFile();
            if (is_array($temp)) {
                $this->oss_url = $temp['oss-request-url'];
            }
        }
    }
}