<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
    'user.passwordMinLength' => 8,
    'upload_type' => [
        'image' => ['jpg', 'jpeg', 'png', 'svg', 'jp2', 'jpe'],
        'video' => ['mp4', 'flv', 'f4v', 'ogv', 'webm'],
        'docs'  => ['doc', 'docx']
    ]
];
