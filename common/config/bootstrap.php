<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@uploads', dirname(dirname(__DIR__)) . '/uploads');
if (YII_ENV == 'dev') {
    defined('RESOURCE_URL') or define('RESOURCE_URL', 'admin.short-video.com/uploads/');
} else {
    defined('RESOURCE_URL') or define('RESOURCE_URL', 'https://admin.short-video.com/');
}