<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%upload_resource}}".
 *
 * @property int $id
 * @property string $url
 * @property string|null $oss_url
 * @property string|null $dirname
 * @property string|null $cover
 * @property string|null $size
 * @property string|null $mime
 * @property string|null $type
 * @property int|null $sign
 * @property int|null $del_flg
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class UploadResource extends \yii\db\ActiveRecord
{
    public function behaviors(): array
    {
        return [
            'class' => TimestampBehavior::class
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%upload_resource}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['url'], 'required'],
            [['sign', 'del_flg', 'created_at', 'updated_at'], 'integer'],
            [['url', 'oss_url', 'dirname', 'cover', 'size', 'mime', 'type'], 'string', 'max' => 255],
            [['sign'], 'default', 'value' => 0]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'url' => '资源路径',
            'oss_url' => 'oss资源路径',
            'dirname' => '物理路径',
            'cover' => '视频封面',
            'size' => '资源尺寸',
            'mime' => 'MIME',
            'type' => '上传类型',
            'sign' => '资源标识',
            'del_flg' => '删除',
            'created_at' => '创建时间',
            'updated_at' => '更新时间',
        ];
    }

    public function fields(): array
    {
        return [
            'id',
            'url',
            'oss_url',
            'cover',
            'type'
        ];
    }

    /**
     * 保存资源
     * @param $params
     * @return UploadResource|false
     */
    public static function saveResource($params)
    {
        $model = new self();

        $model->load($params, '');

        if (!$model->save(false)) {
            return false;
        }
        return $model;
    }
}
