<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%upload_middle}}".
 *
 * @property int $id
 * @property int $upload_id
 * @property int $middle_id
 * @property string $sign
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class UploadMiddle extends \yii\db\ActiveRecord
{
    public function behaviors(): array
    {
        return [
            'class' => TimestampBehavior::class
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%upload_middle}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['upload_id', 'middle_id', 'sign'], 'required'],
            [['upload_id', 'middle_id', 'created_at', 'updated_at'], 'integer'],
            [['sign'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'upload_id' => '资源',
            'middle_id' => '关联',
            'sign' => '标识',
            'created_at' => '创建时间',
            'updated_at' => '更新时间',
        ];
    }

    /**
     * @throws \yii\db\Exception
     */
    public static function saveMiddle($upload_container, $middle_id, $sign): bool
    {
        if (empty($upload_container)) {
            return true;
        }
        self::deleteAll(['sign' => $sign, 'middle_id' => $middle_id]);

        $insert = [];
        array_walk($upload_container, function ($item,$key) use (&$insert,$middle_id,$sign) {
            $insert[$key]['upload_id'] = $item;
            $insert[$key]['middle_id'] = $middle_id;
            $insert[$key]['sign'] = $sign;
            $insert[$key]['created_at'] = time();
            $insert[$key]['updated_at'] = time();
        });

        $result = Yii::$app->db->createCommand()->batchInsert(self::tableName(), [
            'upload_id', 'middle_id', 'sign', 'created_at', 'updated_at'
        ], $insert)->execute();
        return $result ?: false;
    }
}
