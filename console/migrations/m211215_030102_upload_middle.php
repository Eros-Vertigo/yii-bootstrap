<?php

use yii\db\Migration;

/**
 * Class m211215_030102_upload_middle
 */
class m211215_030102_upload_middle extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("{{%upload_middle}}", [
            'id'  => $this->primaryKey(),
            'upload_id' => $this->integer()->notNull()->comment('资源'),
            'middle_id' => $this->integer()->notNull()->comment('关联'),
            'sign' => $this->string(32)->notNull()->comment('标识'),
            'created_at' => $this->integer()->comment('创建时间'),
            'updated_at' => $this->integer()->comment('更新时间')
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB COMMENT="资源中间表"');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{%upload_middle}}");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211215_030102_upload_middle cannot be reverted.\n";

        return false;
    }
    */
}
