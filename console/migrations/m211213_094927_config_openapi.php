<?php

use yii\db\Migration;

/**
 * Class m211213_094927_config_openapi
 */
class m211213_094927_config_openapi extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("{{%config_openapi}}", [
            'id'  => $this->primaryKey(),
            'key' => $this->string()->notNull()->comment('键名'),
            'value' => $this->string()->comment('键值'),
            'secret' => $this->string()->comment('密钥'),
            'remark' => $this->string(200)->comment('备注'),
            'created_at' => $this->integer()->comment('创建时间'),
            'updated_at' => $this->integer()->comment('更新时间')
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB COMMENT="第三方配置表"');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{%config_openapi}}");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211213_094927_config_openapi cannot be reverted.\n";

        return false;
    }
    */
}
