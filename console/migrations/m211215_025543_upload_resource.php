<?php

use yii\db\Migration;

/**
 * Class m211215_025543_upload_resource
 */
class m211215_025543_upload_resource extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("{{%upload_resource}}", [
            'id'  => $this->primaryKey(),
            'url' => $this->string()->notNull()->comment('资源路径'),
            'oss_url' => $this->string()->comment('oss资源路径'),
            'dirname' => $this->string()->comment('物理路径'),
            'cover' => $this->string()->comment('视频封面'),
            'size' => $this->string()->comment('资源尺寸'),
            'mime' => $this->string()->comment('MIME'),
            'type' => $this->string()->comment('上传类型'),
            'sign' => $this->tinyInteger(1)->defaultValue(0)->comment('资源标识'),
            'del_flg' => $this->tinyInteger(1)->defaultValue(0)->comment('删除'),
            'created_at' => $this->integer()->comment('创建时间'),
            'updated_at' => $this->integer()->comment('更新时间')
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB COMMENT="上传资源表"');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{%upload_resource}}");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211215_025543_upload_resource cannot be reverted.\n";

        return false;
    }
    */
}
