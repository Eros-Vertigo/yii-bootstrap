<?php

use yii\db\Migration;

/**
 * Class m211208_093500_verify_code
 */
class m211208_093500_verify_code extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("{{%verify_code}}", [
            'id' => $this->primaryKey(),
            'mobile' => $this->string('11')->notNull()->comment('手机号'),
            'code' => $this->string('32')->notNull()->comment('验证码'),
            'expire_date' => $this->integer()->comment('过期时间'),
            'use_flg' => $this->tinyInteger()->defaultValue(0)->comment('使用'),
            'del_flg' => $this->tinyInteger()->defaultValue(0)->comment('删除'),
            'created_at' => $this->integer()->comment('创建时间'),
            'updated_at' => $this->integer()->comment('更新时间')
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{%verify_code}}");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211208_093500_verify_code cannot be reverted.\n";

        return false;
    }
    */
}
