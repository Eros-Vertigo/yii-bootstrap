<?php

use yii\db\Migration;

/**
 * Class m211209_025910_record_like
 */
class m211209_025910_record_like extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("{{%record_like}}", [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('用户'),
            'sign_id' => $this->integer()->comment('点赞'),
            'sign'    => $this->string('32')->comment('标识'),
            'del_flg' => $this->tinyInteger()->defaultValue(0)->comment('删除'),
            'created_at' => $this->integer()->comment('创建时间'),
            'updated_at' => $this->integer()->comment('更新时间')
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB COMMENT="点赞记录表"');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{%record_like}}");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211209_025910_record_like cannot be reverted.\n";

        return false;
    }
    */
}
