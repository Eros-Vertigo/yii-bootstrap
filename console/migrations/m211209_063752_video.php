<?php

use yii\db\Migration;

/**
 * Class m211209_063752_video
 */
class m211209_063752_video extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("{{%video}}", [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull()->comment('用户'),
            'title' => $this->string()->comment('标题'),
            'tag' => $this->string()->comment('标签'),
            'video_id' => $this->integer()->notNull()->comment('视频链接'),
            'video_mix_url' => $this->string()->comment('视频融合链接'),
            'like_num' => $this->integer()->defaultValue(0)->comment('点赞数'),
            'mix_num' => $this->integer()->defaultValue(0)->comment('融合数'),
            'share_num' => $this->integer()->defaultValue(0)->comment('分享数'),
            'status' => $this->tinyInteger()->defaultValue(0)->comment('状态'),
            'del_flg' => $this->tinyInteger()->defaultValue(0)->comment('删除'),
            'created_at' => $this->integer()->comment('创建时间'),
            'updated_at' => $this->integer()->comment('更新时间')
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB COMMENT="视频表"');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{%video}}");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211209_063752_video cannot be reverted.\n";

        return false;
    }
    */
}
