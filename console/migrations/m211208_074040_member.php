<?php

use yii\db\Migration;

/**
 * Class m211208_074040_member
 */
class m211208_074040_member extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("{{%member}}", [
            'user_id' => $this->primaryKey(),
            'open_id' => $this->string('128')->comment('第三方标识'),
            'username' => $this->string('32')->comment('用户名'),
            'nickname' => $this->string('32')->comment('昵称'),
            'mobile' => $this->string('11')->comment('手机号'),
            'password' => $this->string()->comment('密码'),
            'avatar' => $this->string()->comment('头像'),
            'province' => $this->string(),
            'city' => $this->string(),
            'region' => $this->string(),
            'balance' => $this->decimal(10, 2)->comment('余额'),
            'salt' => $this->string('8')->comment('盐值｜邀请码'),
            'fans_num' => $this->integer()->comment('粉丝数'),
            'focs_num' => $this->integer()->comment('关注数'),
            'like_num' => $this->integer()->comment('点赞数'),
            'del_flg' => $this->tinyInteger()->comment('删除'),
            'vip_flg' => $this->tinyInteger()->comment('vip'),
            'created_at' => $this->integer()->comment('创建时间'),
            'updated_at' => $this->integer()->comment('更新时间')
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB AUTO_INCREMENT=1000');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{%member}}");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211208_074040_member cannot be reverted.\n";

        return false;
    }
    */
}
