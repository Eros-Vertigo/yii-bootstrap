<?php

use yii\db\Migration;

/**
 * Class m211207_090943_menu
 */
class m211207_090943_menu extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("{{%menu}}", [
            'id'        => $this->primaryKey(),
            'name'      => $this->string('32')->comment('菜单名称'),
            'parent'    => $this->integer('11')->comment('父级'),
            'route'     => $this->string('255')->comment('路由'),
            'order'     => $this->integer('11')->comment('排序'),
            'icon'      => $this->string('128')->comment('图标'),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB');

        $this->createIndex("parent", "{{%menu}}", "parent");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{%menu}}");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211207_090943_menu cannot be reverted.\n";

        return false;
    }
    */
}
