<?php

namespace frontend\modules\v1\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%verify_code}}".
 *
 * @property int $id
 * @property string $mobile
 * @property string $code
 * @property int|null $expire_date
 * @property int|null $use_flg
 * @property int|null $del_flg
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class VerifyCode extends \yii\db\ActiveRecord
{
    public function behaviors(): array
    {
        return [
            'class' => TimestampBehavior::class
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%verify_code}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['mobile'], 'required'],
            [['expire_date', 'use_flg', 'del_flg', 'created_at', 'updated_at'], 'integer'],
            [['mobile'], 'string', 'max' => 11],
            [['code'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'mobile' => '手机号',
            'code' => '验证码',
            'expire_date' => '过期时间',
            'use_flg' => '使用',
            'del_flg' => '删除',
            'created_at' => '创建时间',
            'updated_at' => '更新时间',
        ];
    }

    public function fields()
    {
        $fields = parent::fields();
        
        unset($fields['updated_at']);

        return array_merge($fields, [
            'created_at' => function ($model) {
                return Yii::$app->formatter->asDatetime($model->created_at);
            }
        ]);
    }

    public function extraFields(): array
    {
        return [];
    }
}
