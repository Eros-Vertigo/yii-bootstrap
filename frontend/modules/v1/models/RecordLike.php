<?php

namespace frontend\modules\v1\models;

use frontend\components\Common;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%record_like}}".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $sign_id
 * @property string|null $sign
 * @property int|null $del_flg
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class RecordLike extends \yii\db\ActiveRecord
{
    public function behaviors(): array
    {
        return [
            'class' => TimestampBehavior::class
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%record_like}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['user_id', 'sign_id', 'del_flg', 'created_at', 'updated_at'], 'integer'],
            [['sign'], 'string', 'max' => 32],
            [['sign_id'], 'required'],
            [['user_id'], 'default', 'value' => Yii::$app->user->id],
            [['sign'], 'default', 'value' => Member::tableName()]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'user_id' => '用户',
            'sign_id' => '点赞',
            'sign' => '标识',
            'del_flg' => '删除',
            'created_at' => '创建时间',
            'updated_at' => '更新时间',
        ];
    }

    public function fields(): array
    {
        return [
            'id',
            'like_member' => function ($model) {
                return $model->simpleMember;
            },
            'video' => function ($model) {
                return $model->video;
            },
            'created_at' => function ($model) {
                return Yii::$app->formatter->asDatetime($model->created_at);
            }
        ];
    }

    public function extraFields(): array
    {
        return [];
    }

    public function getSimpleMember(): \yii\db\ActiveQuery
    {
        return $this->hasOne(SimpleMember::class, ['user_id' => 'user_id']);
    }

    public function getVideo(): \yii\db\ActiveQuery
    {
        return $this->hasOne(Video::class, ['id' => 'sign_id']);
    }
}
