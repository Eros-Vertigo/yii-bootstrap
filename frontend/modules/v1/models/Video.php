<?php

namespace frontend\modules\v1\models;

use common\models\UploadMiddle;
use common\models\UploadResource;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%video}}".
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $title
 * @property string|null $tag
 * @property string|null $video_cover
 * @property int $video_id
 * @property string|null $video_mix_url
 * @property int|null $like_num
 * @property int|null $mix_num
 * @property int|null $share_num
 * @property int|null $status
 * @property int|null $del_flg
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class Video extends \yii\db\ActiveRecord
{
    public function behaviors(): array
    {
        return [
            'class' => TimestampBehavior::class
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%video}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['video_id'], 'required'],
            [['user_id', 'video_id', 'like_num', 'mix_num', 'share_num', 'status', 'del_flg', 'created_at', 'updated_at'], 'integer'],
            [['title', 'tag', 'video_mix_url'], 'string', 'max' => 255],
            [['user_id'], 'default', 'value' => Yii::$app->user->id],
            [['like_num', 'mix_num', 'share_num'], 'default', 'value' => 0]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'user_id' => '用户',
            'title' => '标题',
            'tag' => '标签',
            'video_id' => '视频',
            'video_mix_url' => '视频融合链接',
            'like_num' => '点赞数',
            'mix_num' => '融合数',
            'share_num' => '分享数',
            'status' => '状态',
            'del_flg' => '删除',
            'created_at' => '创建时间',
            'updated_at' => '更新时间',
        ];
    }

    public function fields(): array
    {
        return [
            'id',
            'user_id',
            'title',
            'tag',
            'video_id',
            'video_mix_url',
            'like_num',
            'mix_num',
            'share_num',
        ];
    }

    public function extraFields(): array
    {
        return ['author', 'video'];
    }

    public function getAuthor(): \yii\db\ActiveQuery
    {
        return $this->hasOne(SimpleMember::class, ['user_id' => 'user_id']);
    }

    public function getVideo(): \yii\db\ActiveQuery
    {
        return $this->hasOne(UploadResource::class, ['id' => 'video_id']);
    }
}
