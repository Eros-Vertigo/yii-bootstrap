<?php
namespace frontend\modules\v1\controllers;

use frontend\controllers\RestFulController;
use frontend\models\VerifyCode;
use yii\data\ActiveDataProvider;
use yii\web\BadRequestHttpException;

class VerifyCodeController extends RestFulController
{
    //模型
    public $modelClass = 'frontend\models\VerifyCode';
    //格式化
    public $serializer = [
        'class'              => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    //行为
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['except'] = ['create'];
        return $behaviors;
    }

    //动作
    public function actions(): array
    {
        $actions = parent::actions();

        unset($actions['create']);
        // 使用 "prepareDataProvider()" 方法自定义数据 provider 
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        return $actions;
    }

    /**
     *
     * @return ActiveDataProvider [type] [description]
     */
    public function prepareDataProvider(): ActiveDataProvider
    {
        $model = new $this->modelClass;

        return new ActiveDataProvider([
            'query' => $model->find()->where(['del_flg' => 0])
        ]);
    }

    /**
     * @throws BadRequestHttpException
     */
    public function actionCreate(): string
    {
        $params = $this->params;

        if (empty($params['mobile'])) {
            throw new BadRequestHttpException("请填写手机号");
        }
        $result = VerifyCode::sendCode($params['mobile']);
        if (is_string($result)) {
            throw new BadRequestHttpException($result);
        }
        return "发送成功";
    }
}
