<?php
namespace frontend\modules\v1\controllers;

use Exception;
use frontend\components\Common;
use frontend\controllers\RestFulController;
use frontend\modules\v1\models\Member;
use frontend\modules\v1\models\Video;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\BadRequestHttpException;

class RecordLikeController extends RestFulController
{
    //模型
    public $modelClass = 'frontend\modules\v1\models\RecordLike';
    //格式化
    public $serializer = [
        'class'              => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    //动作
    public function actions(): array
    {
        $actions = parent::actions();
        unset($actions['create']);
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        return $actions;
    }

    /**
     *
     * @return ActiveDataProvider [type] [description]
     */
    public function prepareDataProvider(): ActiveDataProvider
    {
        $model = new $this->modelClass;

        return new ActiveDataProvider([
            'query' => $model->find()->where(['del_flg' => 0, 'sign' => Yii::$app->user->id])->orderBy('created_at desc')
        ]);
    }

    /**
     * @throws BadRequestHttpException
     */
    public function actionCreate(): array
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $video = Video::findOne($this->params['sign_id']);
            if (empty($video)) {
                throw new Exception("未找到该视频信息,点赞失败");
            }
            $this->params['sign'] = (string)$video->user_id;
            $record = Common::saveEasyModel($this->modelClass, $this->params);
            if (is_string($record)) {
                throw new Exception($record);
            }
            $count = $record->del_flg ? -1 : 1;

            $member_query = Yii::$app->db->createCommand("UPDATE {{%member}} SET like_num = like_num + $count WHERE user_id = $video->user_id")->execute();
            $video_query = Yii::$app->db->createCommand("UPDATE {{%video}} SET like_num = like_num + $count WHERE id = $record->sign_id")->execute();
            if (!$member_query || !$video_query) {
                throw new Exception("点赞失败");
            }
            $transaction->commit();
            $number = Yii::$app->db->createCommand("SELECT like_num FROM {{%video}} WHERE id = $record->sign_id")->queryOne();
            return [
                'flg' => Yii::$app->params['like_status'][$record->del_flg],
                'num' => (int)$number['like_num']
            ];
        } catch (Exception $e) {
            $transaction->rollBack();
            throw new BadRequestHttpException($e->getMessage());
        }
    }
}
