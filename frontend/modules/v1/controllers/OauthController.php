<?php
namespace frontend\modules\v1\controllers;

use OAuth2\Response;
use Yii;
use frontend\controllers\RestFulController;
use frontend\modules\v1\models\Member;
use filsh\yii2\oauth2server\models\OauthAccessTokens;
use yii\base\InvalidConfigException;
use yii\web\BadRequestHttpException;

/**
 * Oauth2授权
 * @author 原桐
 * @version 1.0
 */
class OauthController extends RestFulController
{
    //格式化
    public $serializer = [
        'class'              => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    //行为
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['except'] = ['access-token', 'refresh-token'];
        return $behaviors;
    }

    /**
     * 获取 token
     * @return array [type] [description]
     * @throws InvalidConfigException
     */
    public function actionAccessToken(): array
    {
        Yii::$app->request->setBodyParams(array_merge([
            'grant_type'    => 'password',
            'client_id'     => 'testclient',
            'client_secret' => 'testpass'
        ], Yii::$app->request->getBodyParams()));

        /** @var $response Response */
        $module      = Yii::$app->getModule('oauth2');
        $response    = $module->getServer()->handleTokenRequest();

        return $response->getParameters();
    }

    /**
     * 刷新 token
     * @return array [type] [description]
     * @throws InvalidConfigException
     */
    public function actionRefreshToken(): array
    {
        Yii::$app->request->setBodyParams(array_merge([
            'grant_type'    => 'refresh_token',
            'client_id'     => 'testclient',
            'client_secret' => 'testpass'
        ], Yii::$app->request->getBodyParams()));

        /** @var $response Response */
        $module      = Yii::$app->getModule('oauth2');
        $response    = $module->getServer()->handleTokenRequest();

        $result      = $response->getParameters();
        $oauth_token = OauthAccessTokens::findOne(['access_token' => $result['access_token']]);

        return array_merge($result, [
            'user_id' => $oauth_token->user_id
        ]);
    }
}
