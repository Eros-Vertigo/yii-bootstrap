<?php
namespace frontend\modules\v1\controllers;

use Exception;
use frontend\components\Common;
use frontend\controllers\RestFulController;
use frontend\modules\v1\models\Member;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\BadRequestHttpException;

class RecordFocsController extends RestFulController
{
    //模型
    public $modelClass = 'frontend\modules\v1\models\RecordFocs';
    public string $fansModelClass = 'frontend\modules\v1\models\RecordFans';
    //格式化
    public $serializer = [
        'class'              => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    //动作
    public function actions(): array
    {
        $actions = parent::actions();
        unset($actions['create']);
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        return $actions;
    }

    /**
     *
     * @return ActiveDataProvider [type] [description]
     */
    public function prepareDataProvider(): ActiveDataProvider
    {
        $model = new $this->modelClass;

        return new ActiveDataProvider([
            'query' => $model->find()->where(['del_flg' => 0, 'user_id' => Yii::$app->user->id])->orderBy('updated_at desc')
        ]);
    }

    /**
     * @throws BadRequestHttpException
     */
    public function actionCreate(): array
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $record = Common::saveEasyModel($this->modelClass, $this->params);
            if (is_string($record)) {
                throw new Exception($record);
            }
            $reversal = Common::saveEasyModel($this->fansModelClass, $this->params, true);
            if (is_string($reversal)) {
                throw new Exception($reversal);
            }
            $count = $record->del_flg ? -1 : 1;

            $primary_key = $record->sign == Member::tableName() ? 'user_id' : 'id';

            $focs_query = Yii::$app->db->createCommand("UPDATE $record->sign SET focs_num = focs_num + $count WHERE $primary_key = $record->user_id")->execute();
            $fans_query = Yii::$app->db->createCommand("UPDATE $record->sign SET fans_num = fans_num + $count WHERE $primary_key = $record->sign_id")->execute();
            if (!$focs_query || !$fans_query) {
                throw new Exception("关注失败");
            }
            $transaction->commit();
            $focs_number = Yii::$app->db->createCommand("SELECT focs_num FROM $record->sign WHERE $primary_key = $record->user_id")->queryOne();
            $fans_number = Yii::$app->db->createCommand("SELECT fans_num FROM $record->sign WHERE $primary_key = $record->sign_id")->queryOne();
            return [
                'flg' => Yii::$app->params['focs_status'][$record->del_flg],
                'focs_num' => (int)$focs_number['focs_num'],
                'fans_num' => (int)$fans_number['fans_num'],
            ];
        } catch (Exception $e) {
            $transaction->rollBack();
            throw new BadRequestHttpException($e->getMessage());
        }
    }
}
