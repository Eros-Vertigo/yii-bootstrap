<?php
namespace frontend\modules\v1\controllers;

use frontend\controllers\RestFulController;
use yii\data\ActiveDataProvider;

class MemberController extends RestFulController
{
    //模型
    public $modelClass = 'frontend\modules\v1\models\Member';
    //格式化
    public $serializer = [
        'class'              => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    //行为
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['except'] = ['index'];
        return $behaviors;
    }

    //动作
    public function actions(): array
    {
        $actions = parent::actions();
        // 使用 "prepareDataProvider()" 方法自定义数据 provider 
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        return $actions;
    }

    /**
     *
     * @return ActiveDataProvider [type] [description]
     */
    public function prepareDataProvider(): ActiveDataProvider
    {
        $model = new $this->modelClass;

        return new ActiveDataProvider([
            'query' => $model->find()->where(['del_flg' => 0])
        ]);
    }
}
