<?php
namespace frontend\components;

use common\models\ConfigOpenapi;
use Yii;
/**
 * 公共方法
 * @author 原桐
 */
class Common
{
    /**
     * 简单模型插入
     * @param $modelClass
     * @param $params
     * @param bool $reversal
     * @return false|mixed
     */
    public static function saveEasyModel($modelClass, $params, bool $reversal = false)
    {
        $model = new $modelClass;
        $left_id = $reversal ? $params['sign_id'] : Yii::$app->user->id;
        $right_id = $reversal ? Yii::$app->user->id : $params['sign_id'];
        $model = $model->find()->where(['user_id' => $left_id, 'sign_id' => $right_id])->limit(1)->one();

        if (!empty($model)) {
            $model->del_flg = $model->del_flg ? 0 : 1;
        } else {
            $model = new $modelClass;
            $model->load($params, '');
            $model->del_flg = 0;
        }
        if (!$model->save()) {
            return current($model->getFirstErrors());
        }
        return $model;
    }

    public static function getConfigOpenapi($key): ?ConfigOpenapi
    {
        return ConfigOpenapi::findOne(['key' => $key]);
    }
}