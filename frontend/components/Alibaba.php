<?php

namespace frontend\components;

use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;
use AlibabaCloud\Videoenhan\Videoenhan;
use OSS\Core\OssException;
use OSS\OssClient;
use yii\base\BaseObject;
use yii\web\BadRequestHttpException;

/**
 * 阿里云sdk
 * @author 小原
 */
class Alibaba extends BaseObject
{

    public string $access_key_id;
    public string $access_key_secret;
    /**
     * @var string Oss地域
     */
    public string $endpoint;
    /**
     * @var string Oss储存仓库
     */
    public string $bucket;
    /**
     * @var string Oss存放完整路径
     */
    public string $object;
    /**
     * @var string 本地文件路径
     */
    public string $filePath;
    /**
     * @var string 视频oss链接
     */
    public string $videoUrl;
    /**
     * @var string 待融合人脸oss链接
     */
    public string $referenceURL;
    /**
     * @var string 异步结果id
     */
    public string $jobId;

    /**
     * @throws ClientException
     */
    public function init()
    {
        $this->endpoint = "https://oss-cn-shanghai.aliyuncs.com";
        $config = Common::getConfigOpenapi("alibaba");
        if (!empty($config)) {
            $this->access_key_id = $config->value;
            $this->access_key_secret = $config->secret;
            AlibabaCloud::accessKeyClient($this->access_key_id, $this->access_key_secret)->regionId('cn-shanghai')->asDefaultClient();
        }
    }

    /**
     * 上传oss文件
     */
    public function ossUploadFile()
    {
        try {
            $ossClient = new OssClient($this->access_key_id, $this->access_key_secret, $this->endpoint);
            return $ossClient->uploadFile($this->bucket, $this->object, $this->filePath);
        } catch (OssException $e) {
            return $e->getMessage();
        }
    }

    /**
     * 人脸融合
     * @throws BadRequestHttpException
     */
    public function videoEnhance(): array
    {

        try {
            $result = AlibabaCloud::rpc()
                ->product('videoenhan')
                // ->scheme('https') // https | http
                ->version('2020-03-20')
                ->action('MergeVideoFace')
                ->method('POST')
                ->host('videoenhan.cn-shanghai.aliyuncs.com')
                ->options([
                    'query' => [
                        'Action' => 'MergeVideoFace',
                        'RegionId' => "cn-shanghai",
                        'VideoURL' => $this->videoUrl,
                        'PostURL' => " ",
                        'ReferenceURL' => $this->referenceURL,
                    ],
                ])
                ->request();
            return $result->toArray();
        } catch (ClientException | ServerException $e) {
            throw new BadRequestHttpException($e->getErrorMessage());
        }
    }

    /**
     * 获取异步回调结果
     * @return array
     */
    public function getAsyncResult(): array
    {
        try {
            $request = Videoenhan::v20200320()->getAsyncJobResult();
            $result = $request
                ->withJobId($this->jobId)
                ->connectTimeout(1)
                ->timeout(1)
                ->request();
            $temp = $result->toArray();
            if (array_key_exists('Data', $temp)) {
                $temp['Data']['Result'] = json_decode($temp['Data']['Result']);
            }
            return $temp;
        } catch (ClientException $exception) {
            return ['msg' => $exception->getMessage()];
        } catch (ServerException $exception) {
            return [
                'code' => $exception->getErrorCode(),
                'msg' => $exception->getMessage(),
                'requestId' => $exception->getRequestId(),
                'errs' => $exception->getErrorMessage()
            ];
        }
    }
}