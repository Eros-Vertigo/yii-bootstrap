<?php

use yii\rest\UrlRule;

return [
    [
        'class' => UrlRule::class,
        'controller' => ['v1/oauth'],
        'extraPatterns' => [
            'POST access-token' => 'access-token',
            'POST refresh-token' => 'refresh-token',
        ],
    ],
    [
        'class' => UrlRule::class,
        'controller' => [
            'v1/member',
            'v1/video',
            'v1/verify-code',
            'v1/record-like',
            'v1/record-focs',
        ]
    ],
    "<controller:\w+>/<id:\d+>" => "<controller>/view",
    "<controller:\w+>/<action:\w+>" => "<controller>/<action>",
    "POST upload/upload-image" => "upload/upload-image",
    "POST upload/upload-video" => "upload/upload-video",
];