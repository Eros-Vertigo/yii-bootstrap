<?php
return [
    'adminEmail' => 'admin@example.com',
    'fans_status' => [
        '0' => '关注成功',
        '1' => '取消关注'
    ],
    'focs_status' => [
        '0' => '关注成功',
        '1' => '取消关注'
    ],
    'like_status' => [
        '0' => '点赞成功',
        '1' => '取消点赞'
    ]
];
