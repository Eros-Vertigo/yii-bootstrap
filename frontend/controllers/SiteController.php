<?php

namespace frontend\controllers;

use frontend\components\Alibaba;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions(): array
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'test' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @throws BadRequestHttpException
     */
    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            throw new BadRequestHttpException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function actionTest()
    {
        $object = Yii::createObject([
            'class' => Alibaba::class,
            'jobId' => 'A6A6C842-D499-5F9D-9946-6F1DECF6AC68'
        ]);
        $result = $object->getAsyncResult();

        return $result;
//        var_dump($result);exit();
    }
}
