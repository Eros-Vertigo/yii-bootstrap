<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidConfigException;
use yii\web\Controller;
use common\components\UploadCommon;

class UploadController extends Controller
{
    /**
     * 上传图片
     * @throws InvalidConfigException
     */
    public function actionUploadImage()
    {
        $upload = Yii::createObject([
            'class' => UploadCommon::class,
            'upload_type' => 'image',
            'oss_flg' => true
        ]);
        return $upload->upload();
    }

    /**
     * 上传视频
     * @throws InvalidConfigException
     */
    public function actionUploadVideo()
    {
        $upload = Yii::createObject([
            'class' => UploadCommon::class,
            'upload_type' => 'video',
            'oss_flg' => true
        ]);
        return $upload->upload();
    }
}