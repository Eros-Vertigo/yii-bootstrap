<?php 
namespace frontend\controllers;

use Yii;
use yii\base\InvalidConfigException;
use yii\filters\Cors;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBearerAuth;
use filsh\yii2\oauth2server\filters\auth\CompositeAuth;

class RestFulController extends ActiveController
{
    protected array $params;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        $this->params = Yii::$app->request->getBodyParams();
    }

	public function behaviors(): array
    {
		$behaviors = parent::behaviors();

		$behaviors = array_merge([
			'corsFilter' => [
				'class' => Cors::class,
			],
//			'exceptionFilter' => [
//                'class' => ErrorToExceptionFilter::className()
//            ],
		], $behaviors);

		$behaviors['authenticator'] = [
			'class'       => CompositeAuth::class,
			'authMethods' => [
                ['class' => HttpBearerAuth::class],
            ]
		];
		$behaviors['authenticator']['except'] = ['options'];


		return $behaviors;
	}

}