<?php

namespace frontend\models;

use Exception;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "verify_code".
 *
 * @property int $id
 * @property string $mobile
 * @property string $code
 * @property int|null $expire_date
 * @property int|null $use_flg
 * @property int|null $del_flg
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class VerifyCode extends ActiveRecord
{
    public function behaviors(): array
    {
        return [
            ['class' => TimestampBehavior::class],
            [
                'class' => AttributeBehavior::class,
                'attributes' => [
                    parent::EVENT_BEFORE_INSERT => ['expire_date'],
                ],
                'value' => function ($event) {
                    return strtotime("+5 minute");
                }
            ]
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return "{{%verify_code}}";
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['mobile'], 'required'],
            [['expire_date', 'use_flg', 'del_flg', 'created_at', 'updated_at'], 'integer'],
            [['mobile'], 'string', 'max' => 11],
            [['code'], 'string', 'max' => 32],
            [['mobile'], 'match', 'pattern' => '/^1[3|4|5|7|8][0-9]{9}$/', 'message' => '请输入正确的手机号'],
            [['code'], 'default', 'value' => '123123']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'mobile' => '手机号',
            'code' => '验证码',
            'expire_date' => '过期时间',
            'use_flg' => '使用',
            'del_flg' => '删除',
            'created_at' => '创建时间',
            'updated_at' => '更新时间',
        ];
    }

    /**
     * 发送验证码
     * @param $mobile
     * @return false|VerifyCode|mixed
     */
    public static function sendCode($mobile)
    {
        $temp = self::find()->where(['use_flg' => 0, 'mobile' => $mobile])->andWhere(['<', time(), new Expression('created_at+60')])
            ->limit(1)->one();
        if (!empty($temp)) {
            return"请勿重复发送";
        }
        $model = new self();
        $model->mobile = $mobile;
        return $model->save() ? $model : current($model->getFirstErrors());
    }

    /**
     * check 验证码
     * @param $username
     * @param $password
     * @return false|string
     */
    public static function checkCode($username, $password)
    {
        try {
            $record = self::find()->where(['mobile' => $username, 'use_flg' => 0])->orderBy('id desc')->limit(1)->one();
            if (empty($record)) {
                throw new Exception ("请先发送验证码");
            }
            if ($record->code != $password) {
                throw new Exception("验证码错误");
            }
            if ($record->expire_date < time()) {
                throw new Exception("验证码已过期");
            }
            return false;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * 删除验证码
     * @param $username
     * @param $code
     */
    public static function delCode($username, $code)
    {
        $record = self::findOne(['mobile' => $username, 'code' => $code]);
        $record->use_flg = 1;
        $record->save();
    }
}
