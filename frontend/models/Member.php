<?php

namespace frontend\models;

use filsh\yii2\oauth2server\models\OauthAccessTokens;
use OAuth2\Storage\UserCredentialsInterface;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\BadRequestHttpException;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "{{%member}}".
 *
 * @property int $user_id
 * @property string|null $username 用户名
 * @property string|null $nickname 昵称
 * @property string|null $mobile 手机号
 * @property string|null $password 密码
 * @property string|null $avatar 头像
 * @property string|null $province
 * @property string|null $city
 * @property string|null $region
 * @property float|null $balance 余额
 * @property string|null $salt 盐值｜邀请码
 * @property int|null $fans_num 粉丝数
 * @property int|null $focs_num 关注数
 * @property int|null $like_num 点赞数
 * @property int|null $del_flg 删除
 * @property int|null $vip_flg vip
 * @property int|null $created_at 创建时间
 * @property int|null $updated_at 更新时间
 */
class Member extends ActiveRecord implements IdentityInterface, UserCredentialsInterface
{
    public function behaviors(): array
    {
        return [
            ['class' => TimestampBehavior::class],
            [
                'class' => AttributeBehavior::class,
                'attributes' => [
                    parent::EVENT_BEFORE_INSERT => ['expire_date'],
                ],
                'value' => function ($event) {
                    return strtotime("+5 minute");
                }
            ]
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%member}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['balance'], 'number'],
            [['fans_num', 'focs_num', 'like_num', 'del_flg', 'vip_flg', 'created_at', 'updated_at'], 'integer'],
            [['username', 'nickname'], 'string', 'max' => 32],
            [['mobile'], 'string', 'max' => 11],
            [['password', 'avatar', 'province', 'city', 'region'], 'string', 'max' => 255],
            [['salt'], 'string', 'max' => 8],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'user_id' => 'User ID',
            'username' => '用户名',
            'nickname' => '昵称',
            'mobile' => '手机号',
            'password' => '密码',
            'avatar' => '头像',
            'province' => 'Province',
            'city' => 'City',
            'region' => 'Region',
            'balance' => '余额',
            'salt' => '盐值｜邀请码',
            'fans_num' => '粉丝数',
            'focs_num' => '关注数',
            'like_num' => '点赞数',
            'del_flg' => '删除',
            'vip_flg' => 'vip',
            'created_at' => '创建时间',
            'updated_at' => '更新时间',
        ];
    }

    /**
     * @param int|string $id
     * @return IdentityInterface|null
     */
    public static function findIdentity($id): ?IdentityInterface
    {
        return static::findOne($id);
    }

    /**
     * @param mixed $token
     * @param mixed|null $type
     * @return IdentityInterface|null
     */
    public static function findIdentityByAccessToken($token, $type = null): ?IdentityInterface
    {
        // TODO: Implement findIdentityByAccessToken() method.
        $oauth_token = OauthAccessTokens::findOne(['access_token' => $token]);
        return static::findIdentity($oauth_token->user_id);
    }

    /**
     * @return int|string
     */
    public function getId()
    {
        // TODO: Implement getId() method.
        return $this->user_id;
    }

    /**
     * @return void
     */
    public function getAuthKey(): void
    {
        // TODO: Implement getAuthKey() method.
    }

    /**
     * @param string $authKey
     * @return void
     */
    public function validateAuthKey($authKey): void
    {
        // TODO: Implement validateAuthKey() method.
    }

    /**
     * @param $username
     * @param $password
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function checkUserCredentials($username, $password)
    {
        // TODO: Implement checkUserCredentials() method.
        $type   = Yii::$app->request->getBodyParam('type');

        if ($type == 'wechat') {
            $member = $this->findByOpenid($username);
        } else {
            $member = $this->findByMobile($username);
        }

        if (empty($member)) {
            self::createMember($username);
        }
        if ($type == 'wechat') { return true; }
        if ($type == 'password') {
            $check = Yii::$app->security->validatePassword($password, $member->password);
            if (!$check) {
                throw new BadRequestHttpException("密码错误");
            }
            return true;
        } else {
            $checkCode = VerifyCode::checkCode($username, $password);

            if ($checkCode === false) {
                VerifyCode::delCode($username, $password);
                return true;
            } else {
                throw new BadRequestHttpException($checkCode);
            }
        }
    }

    /**
     * @param string $username
     * @return array|false
     */
    public function getUserDetails($username)
    {
        // TODO: Implement getUserDetails() method.
        if (Yii::$app->request->getBodyParam('type') == 'wechat') {
            $member = $this->findByOpenid($username);
        } else {
            $member = $this->findByMobile($username);
        }

        return ['user_id' => $member->user_id ?? '', 'scope' => ''];
    }

    private function findByOpenid(string $username): ?Member
    {
        return self::findOne(['openid' => $username]);
    }

    private function findByMobile(string $username): ?Member
    {
        return self::findOne(['username' => $username]);
    }

    private static function createMember($mobile): bool
    {
        $model = new self();
        $model->username = $mobile;
        $model->mobile = $mobile;
        return $model->save();
    }
}
