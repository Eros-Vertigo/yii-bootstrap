<?php return array(
    'root' => array(
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => 'yiisoft/yii2-app-advanced',
        'dev' => true,
    ),
    'versions' => array(
        'adbario/php-dot-notation' => array(
            'pretty_version' => '2.2.0',
            'version' => '2.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../adbario/php-dot-notation',
            'aliases' => array(),
            'reference' => 'eee4fc81296531e6aafba4c2bbccfc5adab1676e',
            'dev_requirement' => false,
        ),
        'alchemy/binary-driver' => array(
            'pretty_version' => 'v5.2.0',
            'version' => '5.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../alchemy/binary-driver',
            'aliases' => array(),
            'reference' => 'e0615cdff315e6b4b05ada67906df6262a020d22',
            'dev_requirement' => false,
        ),
        'alibabacloud/aas' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/actiontrail' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/adb' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/aegis' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/afs' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/airec' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/alidns' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/alikafka' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/alimt' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/aliprobe' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/aliyuncvc' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/appmallsservice' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/arms' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/arms4finance' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/baas' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/batchcompute' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/bss' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/bssopenapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/cas' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/cbn' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/ccc' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/ccs' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/cdn' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/cds' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/cf' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/chatbot' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/client' => array(
            'pretty_version' => '1.5.31',
            'version' => '1.5.31.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../alibabacloud/client',
            'aliases' => array(),
            'reference' => '19224d92fe27ab8ef501d77d4891e7660bc023c1',
            'dev_requirement' => false,
        ),
        'alibabacloud/cloudapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/cloudauth' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/cloudesl' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/cloudmarketing' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/cloudphoto' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/cloudwf' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/cms' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/commondriver' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/companyreg' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/cr' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/crm' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/cs' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/csb' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/cusanalyticsconline' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/dataworkspublic' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/dbs' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/dcdn' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/dds' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/democenter' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/dm' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/dmsenterprise' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/domain' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/domainintl' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/drcloud' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/drds' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/dts' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/dybaseapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/dyplsapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/dypnsapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/dysmsapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/dyvmsapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/eci' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/ecs' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/ecsinc' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/edas' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/ehpc' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/elasticsearch' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/emr' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/ess' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/facebody' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/fnf' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/foas' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/ft' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/goodstech' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/gpdb' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/green' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/hbase' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/hiknoengine' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/hpc' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/hsm' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/httpdns' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/idst' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/imageaudit' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/imageenhan' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/imagerecog' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/imagesearch' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/imageseg' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/imm' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/industrybrain' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/iot' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/iqa' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/itaas' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/ivision' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/ivpd' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/jaq' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/jarvis' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/jarvispublic' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/kms' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/linkedmall' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/linkface' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/linkwan' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/live' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/lubancloud' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/lubanruler' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/market' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/mopen' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/mpserverless' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/mts' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/multimediaai' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/nas' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/netana' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/nlp' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/nlpautoml' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/nlscloudmeta' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/nlsfiletrans' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/objectdet' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/ocr' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/ocs' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/oms' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/ons' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/onsmqtt' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/oos' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/openanalytics' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/ossadmin' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/ots' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/outboundbot' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/petadata' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/polardb' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/productcatalog' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/pts' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/push' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/pvtz' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/qualitycheck' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/ram' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/rds' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/reid' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/retailcloud' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/rkvstore' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/ros' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/rtc' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/saf' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/sas' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/sasapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/scdn' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/schedulerx2' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/sdk' => array(
            'pretty_version' => '1.8.1183',
            'version' => '1.8.1183.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../alibabacloud/sdk',
            'aliases' => array(),
            'reference' => '970b69c756cdc0dbf0d9300611d2042bae76c92d',
            'dev_requirement' => false,
        ),
        'alibabacloud/skyeye' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/slb' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/smartag' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/smc' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/sms' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/smsintl' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/snsuapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/sts' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/taginner' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/tesladam' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/teslamaxcompute' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/teslastream' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/ubsms' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/ubsmsinner' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/uis' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/unimkt' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/visionai' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/vod' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/voicenavigator' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/vpc' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/vs' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/wafopenapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/welfareinner' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/xspace' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/xtrace' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/yqbridge' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'alibabacloud/yundun' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1183',
            ),
        ),
        'aliyuncs/oss-sdk-php' => array(
            'pretty_version' => 'v2.4.3',
            'version' => '2.4.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../aliyuncs/oss-sdk-php',
            'aliases' => array(),
            'reference' => '4ccead614915ee6685bf30016afb01aabd347e46',
            'dev_requirement' => false,
        ),
        'behat/gherkin' => array(
            'pretty_version' => 'v4.9.0',
            'version' => '4.9.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../behat/gherkin',
            'aliases' => array(),
            'reference' => '0bc8d1e30e96183e4f36db9dc79caead300beff4',
            'dev_requirement' => true,
        ),
        'bower-asset/inputmask' => array(
            'pretty_version' => '3.3.11',
            'version' => '3.3.11.0',
            'type' => 'bower-asset',
            'install_path' => __DIR__ . '/../bower-asset/inputmask',
            'aliases' => array(),
            'reference' => '5e670ad62f50c738388d4dcec78d2888505ad77b',
            'dev_requirement' => false,
        ),
        'bower-asset/jquery' => array(
            'pretty_version' => '3.6.0',
            'version' => '3.6.0.0',
            'type' => 'bower-asset',
            'install_path' => __DIR__ . '/../bower-asset/jquery',
            'aliases' => array(),
            'reference' => 'e786e3d9707ffd9b0dd330ca135b66344dcef85a',
            'dev_requirement' => false,
        ),
        'bower-asset/punycode' => array(
            'pretty_version' => 'v1.3.2',
            'version' => '1.3.2.0',
            'type' => 'bower-asset',
            'install_path' => __DIR__ . '/../bower-asset/punycode',
            'aliases' => array(),
            'reference' => '38c8d3131a82567bfef18da09f7f4db68c84f8a3',
            'dev_requirement' => false,
        ),
        'bower-asset/yii2-pjax' => array(
            'pretty_version' => '2.0.7.1',
            'version' => '2.0.7.1',
            'type' => 'bower-asset',
            'install_path' => __DIR__ . '/../bower-asset/yii2-pjax',
            'aliases' => array(),
            'reference' => 'aef7b953107264f00234902a3880eb50dafc48be',
            'dev_requirement' => false,
        ),
        'bshaffer/oauth2-server-php' => array(
            'pretty_version' => 'v1.11.1',
            'version' => '1.11.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../bshaffer/oauth2-server-php',
            'aliases' => array(),
            'reference' => '5a0c8000d4763b276919e2106f54eddda6bc50fa',
            'dev_requirement' => false,
        ),
        'cebe/markdown' => array(
            'pretty_version' => '1.2.1',
            'version' => '1.2.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../cebe/markdown',
            'aliases' => array(),
            'reference' => '9bac5e971dd391e2802dca5400bbeacbaea9eb86',
            'dev_requirement' => false,
        ),
        'clagiordano/weblibs-configmanager' => array(
            'pretty_version' => 'v1.2.0',
            'version' => '1.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../clagiordano/weblibs-configmanager',
            'aliases' => array(),
            'reference' => '5c8ebcc62782313b1278afe802b120d18c07a059',
            'dev_requirement' => false,
        ),
        'codeception/codeception' => array(
            'pretty_version' => '4.1.22',
            'version' => '4.1.22.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../codeception/codeception',
            'aliases' => array(),
            'reference' => '9777ec3690ceedc4bce2ed13af7af4ca4ee3088f',
            'dev_requirement' => true,
        ),
        'codeception/lib-asserts' => array(
            'pretty_version' => '1.13.2',
            'version' => '1.13.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../codeception/lib-asserts',
            'aliases' => array(),
            'reference' => '184231d5eab66bc69afd6b9429344d80c67a33b6',
            'dev_requirement' => true,
        ),
        'codeception/lib-innerbrowser' => array(
            'pretty_version' => '1.5.1',
            'version' => '1.5.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../codeception/lib-innerbrowser',
            'aliases' => array(),
            'reference' => '31b4b56ad53c3464fcb2c0a14d55a51a201bd3c2',
            'dev_requirement' => true,
        ),
        'codeception/module-asserts' => array(
            'pretty_version' => '1.3.1',
            'version' => '1.3.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../codeception/module-asserts',
            'aliases' => array(),
            'reference' => '59374f2fef0cabb9e8ddb53277e85cdca74328de',
            'dev_requirement' => true,
        ),
        'codeception/module-filesystem' => array(
            'pretty_version' => '1.0.3',
            'version' => '1.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../codeception/module-filesystem',
            'aliases' => array(),
            'reference' => '781be167fb1557bfc9b61e0a4eac60a32c534ec1',
            'dev_requirement' => true,
        ),
        'codeception/module-yii2' => array(
            'pretty_version' => '1.1.4',
            'version' => '1.1.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../codeception/module-yii2',
            'aliases' => array(),
            'reference' => 'aec2976c45136056f1b05c930cd376bb283d42e2',
            'dev_requirement' => true,
        ),
        'codeception/phpunit-wrapper' => array(
            'pretty_version' => '8.1.4',
            'version' => '8.1.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../codeception/phpunit-wrapper',
            'aliases' => array(),
            'reference' => 'f41335f0b4dd17cf7bbc63e87943b3ae72a8bbc3',
            'dev_requirement' => true,
        ),
        'codeception/stub' => array(
            'pretty_version' => '3.7.0',
            'version' => '3.7.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../codeception/stub',
            'aliases' => array(),
            'reference' => '468dd5fe659f131fc997f5196aad87512f9b1304',
            'dev_requirement' => true,
        ),
        'codeception/verify' => array(
            'pretty_version' => '1.1.0',
            'version' => '1.1.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../codeception/verify',
            'aliases' => array(),
            'reference' => 'fa0bb946b6d61279f461bcc5a677ac0ed5eab9b3',
            'dev_requirement' => true,
        ),
        'danielstjules/stringy' => array(
            'pretty_version' => '3.1.0',
            'version' => '3.1.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../danielstjules/stringy',
            'aliases' => array(),
            'reference' => 'df24ab62d2d8213bbbe88cc36fc35a4503b4bd7e',
            'dev_requirement' => false,
        ),
        'doctrine/cache' => array(
            'pretty_version' => '1.12.1',
            'version' => '1.12.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/cache',
            'aliases' => array(),
            'reference' => '4cf401d14df219fa6f38b671f5493449151c9ad8',
            'dev_requirement' => false,
        ),
        'doctrine/instantiator' => array(
            'pretty_version' => '1.4.0',
            'version' => '1.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/instantiator',
            'aliases' => array(),
            'reference' => 'd56bf6102915de5702778fe20f2de3b2fe570b5b',
            'dev_requirement' => true,
        ),
        'doctrine/lexer' => array(
            'pretty_version' => '1.2.1',
            'version' => '1.2.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/lexer',
            'aliases' => array(),
            'reference' => 'e864bbf5904cb8f5bb334f99209b48018522f042',
            'dev_requirement' => false,
        ),
        'egulias/email-validator' => array(
            'pretty_version' => '3.1.2',
            'version' => '3.1.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../egulias/email-validator',
            'aliases' => array(),
            'reference' => 'ee0db30118f661fb166bcffbf5d82032df484697',
            'dev_requirement' => false,
        ),
        'evenement/evenement' => array(
            'pretty_version' => 'v3.0.1',
            'version' => '3.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../evenement/evenement',
            'aliases' => array(),
            'reference' => '531bfb9d15f8aa57454f5f0285b18bec903b8fb7',
            'dev_requirement' => false,
        ),
        'ezyang/htmlpurifier' => array(
            'pretty_version' => 'v4.13.0',
            'version' => '4.13.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ezyang/htmlpurifier',
            'aliases' => array(),
            'reference' => '08e27c97e4c6ed02f37c5b2b20488046c8d90d75',
            'dev_requirement' => false,
        ),
        'fakerphp/faker' => array(
            'pretty_version' => 'v1.17.0',
            'version' => '1.17.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../fakerphp/faker',
            'aliases' => array(),
            'reference' => 'b85e9d44eae8c52cca7aa0939483611f7232b669',
            'dev_requirement' => true,
        ),
        'filsh/yii2-oauth2-server' => array(
            'pretty_version' => 'v2.1.1',
            'version' => '2.1.1.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../filsh/yii2-oauth2-server',
            'aliases' => array(),
            'reference' => '3934cec6d6de9eb21d7a6ef5b31cadac281f3ad2',
            'dev_requirement' => false,
        ),
        'fortawesome/font-awesome' => array(
            'pretty_version' => '5.15.4',
            'version' => '5.15.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../fortawesome/font-awesome',
            'aliases' => array(),
            'reference' => '7d3d774145ac38663f6d1effc6def0334b68ab7e',
            'dev_requirement' => false,
        ),
        'guzzlehttp/guzzle' => array(
            'pretty_version' => '7.4.1',
            'version' => '7.4.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/guzzle',
            'aliases' => array(),
            'reference' => 'ee0a041b1760e6a53d2a39c8c34115adc2af2c79',
            'dev_requirement' => false,
        ),
        'guzzlehttp/promises' => array(
            'pretty_version' => '1.5.1',
            'version' => '1.5.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/promises',
            'aliases' => array(),
            'reference' => 'fe752aedc9fd8fcca3fe7ad05d419d32998a06da',
            'dev_requirement' => false,
        ),
        'guzzlehttp/psr7' => array(
            'pretty_version' => '2.1.0',
            'version' => '2.1.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/psr7',
            'aliases' => array(),
            'reference' => '089edd38f5b8abba6cb01567c2a8aaa47cec4c72',
            'dev_requirement' => false,
        ),
        'kartik-v/bootstrap-fileinput' => array(
            'pretty_version' => 'v5.2.6',
            'version' => '5.2.6.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../kartik-v/bootstrap-fileinput',
            'aliases' => array(),
            'reference' => '642849327db63231922558b580e985e27beddfc1',
            'dev_requirement' => false,
        ),
        'kartik-v/bootstrap-star-rating' => array(
            'pretty_version' => 'v4.1.2',
            'version' => '4.1.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../kartik-v/bootstrap-star-rating',
            'aliases' => array(),
            'reference' => 'c301efed4c82e9d5f11a0845ae428ba60931b44e',
            'dev_requirement' => false,
        ),
        'kartik-v/dependent-dropdown' => array(
            'pretty_version' => 'v1.4.9',
            'version' => '1.4.9.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../kartik-v/dependent-dropdown',
            'aliases' => array(),
            'reference' => '54a8806002ee21b744508a2edb95ed01d35c6cf9',
            'dev_requirement' => false,
        ),
        'kartik-v/yii2-krajee-base' => array(
            'pretty_version' => 'v3.0.1',
            'version' => '3.0.1.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../kartik-v/yii2-krajee-base',
            'aliases' => array(),
            'reference' => 'bbf7b58b0000f44834c18c0f2eed13a0a7d04c09',
            'dev_requirement' => false,
        ),
        'kartik-v/yii2-widget-activeform' => array(
            'pretty_version' => 'v1.6.0',
            'version' => '1.6.0.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../kartik-v/yii2-widget-activeform',
            'aliases' => array(),
            'reference' => 'd54f986737d4c2b309bc72760f5aa55f78286e0e',
            'dev_requirement' => false,
        ),
        'kartik-v/yii2-widget-affix' => array(
            'pretty_version' => 'v1.0.0',
            'version' => '1.0.0.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../kartik-v/yii2-widget-affix',
            'aliases' => array(),
            'reference' => '2184119bfa518c285406156f744769b13b861712',
            'dev_requirement' => false,
        ),
        'kartik-v/yii2-widget-alert' => array(
            'pretty_version' => 'v1.1.5',
            'version' => '1.1.5.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../kartik-v/yii2-widget-alert',
            'aliases' => array(),
            'reference' => '6a45d7dc294eecd578cf8cb9acb671d1cafa0727',
            'dev_requirement' => false,
        ),
        'kartik-v/yii2-widget-colorinput' => array(
            'pretty_version' => 'v1.0.6',
            'version' => '1.0.6.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../kartik-v/yii2-widget-colorinput',
            'aliases' => array(),
            'reference' => 'e35e6c7615a735b65557d6c38d112b77e2628c69',
            'dev_requirement' => false,
        ),
        'kartik-v/yii2-widget-datepicker' => array(
            'pretty_version' => 'v1.4.8',
            'version' => '1.4.8.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../kartik-v/yii2-widget-datepicker',
            'aliases' => array(),
            'reference' => 'f5f8b396cf03d4a383aad5e7b338f8cb065abf66',
            'dev_requirement' => false,
        ),
        'kartik-v/yii2-widget-datetimepicker' => array(
            'pretty_version' => 'v1.5.0',
            'version' => '1.5.0.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../kartik-v/yii2-widget-datetimepicker',
            'aliases' => array(),
            'reference' => '881985a5e482a4e37d1901c7857912ba5af3f298',
            'dev_requirement' => false,
        ),
        'kartik-v/yii2-widget-depdrop' => array(
            'pretty_version' => 'v1.0.6',
            'version' => '1.0.6.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../kartik-v/yii2-widget-depdrop',
            'aliases' => array(),
            'reference' => 'ea347e3793fbd8273cc9bd1eb94c4b32bb55d318',
            'dev_requirement' => false,
        ),
        'kartik-v/yii2-widget-fileinput' => array(
            'pretty_version' => 'v1.1.0',
            'version' => '1.1.0.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../kartik-v/yii2-widget-fileinput',
            'aliases' => array(),
            'reference' => 'd43bb9d9638ba117bbaa0045250645dc843fcf7f',
            'dev_requirement' => false,
        ),
        'kartik-v/yii2-widget-growl' => array(
            'pretty_version' => 'v1.1.2',
            'version' => '1.1.2.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../kartik-v/yii2-widget-growl',
            'aliases' => array(),
            'reference' => '37e8f9f10d3bc9d71f3ef64c4aaa0e2fc83dd5dc',
            'dev_requirement' => false,
        ),
        'kartik-v/yii2-widget-rangeinput' => array(
            'pretty_version' => 'v1.0.2',
            'version' => '1.0.2.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../kartik-v/yii2-widget-rangeinput',
            'aliases' => array(),
            'reference' => 'dd9019bab7e5bf570a02870d9e74387891bbdb32',
            'dev_requirement' => false,
        ),
        'kartik-v/yii2-widget-rating' => array(
            'pretty_version' => 'v1.0.5',
            'version' => '1.0.5.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../kartik-v/yii2-widget-rating',
            'aliases' => array(),
            'reference' => 'd3d7249490044f80e65f8f3938191f39a76586b2',
            'dev_requirement' => false,
        ),
        'kartik-v/yii2-widget-select2' => array(
            'pretty_version' => 'v2.2.2',
            'version' => '2.2.2.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../kartik-v/yii2-widget-select2',
            'aliases' => array(),
            'reference' => '5b3c91df7908193981033cb7ca52e83303197753',
            'dev_requirement' => false,
        ),
        'kartik-v/yii2-widget-sidenav' => array(
            'pretty_version' => 'v1.0.1',
            'version' => '1.0.1.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../kartik-v/yii2-widget-sidenav',
            'aliases' => array(),
            'reference' => '87e9c815624aa966d70bb4507b3d53c158db0d43',
            'dev_requirement' => false,
        ),
        'kartik-v/yii2-widget-spinner' => array(
            'pretty_version' => 'v1.0.1',
            'version' => '1.0.1.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../kartik-v/yii2-widget-spinner',
            'aliases' => array(),
            'reference' => 'eb10dad17a107bf14f173c99994770ca23c548a6',
            'dev_requirement' => false,
        ),
        'kartik-v/yii2-widget-switchinput' => array(
            'pretty_version' => 'v1.3.1',
            'version' => '1.3.1.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../kartik-v/yii2-widget-switchinput',
            'aliases' => array(),
            'reference' => '7d8ee999d79bcdc1601da5cd59439ac7eb1f5ea6',
            'dev_requirement' => false,
        ),
        'kartik-v/yii2-widget-timepicker' => array(
            'pretty_version' => 'v1.0.5',
            'version' => '1.0.5.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../kartik-v/yii2-widget-timepicker',
            'aliases' => array(),
            'reference' => '680aec2d79846e926c072da455cf6f33e1c3bb12',
            'dev_requirement' => false,
        ),
        'kartik-v/yii2-widget-touchspin' => array(
            'pretty_version' => 'v1.2.4',
            'version' => '1.2.4.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../kartik-v/yii2-widget-touchspin',
            'aliases' => array(),
            'reference' => '1eec4c3f3a8bf9a170e1e0682c2c89f2929d65e9',
            'dev_requirement' => false,
        ),
        'kartik-v/yii2-widget-typeahead' => array(
            'pretty_version' => 'v1.0.4',
            'version' => '1.0.4.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../kartik-v/yii2-widget-typeahead',
            'aliases' => array(),
            'reference' => '7b7041a3cbbeb2db0a608e9f6c9b3f4f63b0069d',
            'dev_requirement' => false,
        ),
        'kartik-v/yii2-widgets' => array(
            'pretty_version' => 'v3.4.1',
            'version' => '3.4.1.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../kartik-v/yii2-widgets',
            'aliases' => array(),
            'reference' => 'e5a030d700243a90eccf96a070380bd3b76e17a3',
            'dev_requirement' => false,
        ),
        'mdmsoft/yii2-admin' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../mdmsoft/yii2-admin',
            'aliases' => array(
                0 => '2.x-dev',
            ),
            'reference' => '10e526fbcf6350ed4cc9ba6298f887d611685367',
            'dev_requirement' => false,
        ),
        'mtdowling/jmespath.php' => array(
            'pretty_version' => '2.6.1',
            'version' => '2.6.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../mtdowling/jmespath.php',
            'aliases' => array(),
            'reference' => '9b87907a81b87bc76d19a7fb2d61e61486ee9edb',
            'dev_requirement' => false,
        ),
        'myclabs/deep-copy' => array(
            'pretty_version' => '1.10.2',
            'version' => '1.10.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../myclabs/deep-copy',
            'aliases' => array(),
            'reference' => '776f831124e9c62e1a2c601ecc52e776d8bb7220',
            'dev_requirement' => true,
            'replaced' => array(
                0 => '1.10.2',
            ),
        ),
        'neutron/temporary-filesystem' => array(
            'pretty_version' => '3.0',
            'version' => '3.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../neutron/temporary-filesystem',
            'aliases' => array(),
            'reference' => '60e79adfd16f42f4b888e351ad49f9dcb959e3c2',
            'dev_requirement' => false,
        ),
        'npm-asset/bootstrap' => array(
            'pretty_version' => '4.6.1',
            'version' => '4.6.1.0',
            'type' => 'npm-asset',
            'install_path' => __DIR__ . '/../npm-asset/bootstrap',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
        'opis/closure' => array(
            'pretty_version' => '3.6.2',
            'version' => '3.6.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../opis/closure',
            'aliases' => array(),
            'reference' => '06e2ebd25f2869e54a306dda991f7db58066f7f6',
            'dev_requirement' => true,
        ),
        'paragonie/random_compat' => array(
            'pretty_version' => 'v9.99.100',
            'version' => '9.99.100.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../paragonie/random_compat',
            'aliases' => array(),
            'reference' => '996434e5492cb4c3edcb9168db6fbb1359ef965a',
            'dev_requirement' => false,
        ),
        'phar-io/manifest' => array(
            'pretty_version' => '2.0.3',
            'version' => '2.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phar-io/manifest',
            'aliases' => array(),
            'reference' => '97803eca37d319dfa7826cc2437fc020857acb53',
            'dev_requirement' => true,
        ),
        'phar-io/version' => array(
            'pretty_version' => '3.1.0',
            'version' => '3.1.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phar-io/version',
            'aliases' => array(),
            'reference' => 'bae7c545bef187884426f042434e561ab1ddb182',
            'dev_requirement' => true,
        ),
        'php-ffmpeg/php-ffmpeg' => array(
            'pretty_version' => 'v0.18.0',
            'version' => '0.18.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-ffmpeg/php-ffmpeg',
            'aliases' => array(),
            'reference' => 'edc0a7729d8818ed883e77b3d26ceb6d49ec41de',
            'dev_requirement' => false,
        ),
        'phpdocumentor/reflection-common' => array(
            'pretty_version' => '2.2.0',
            'version' => '2.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpdocumentor/reflection-common',
            'aliases' => array(),
            'reference' => '1d01c49d4ed62f25aa84a747ad35d5a16924662b',
            'dev_requirement' => true,
        ),
        'phpdocumentor/reflection-docblock' => array(
            'pretty_version' => '5.3.0',
            'version' => '5.3.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpdocumentor/reflection-docblock',
            'aliases' => array(),
            'reference' => '622548b623e81ca6d78b721c5e029f4ce664f170',
            'dev_requirement' => true,
        ),
        'phpdocumentor/type-resolver' => array(
            'pretty_version' => '1.5.1',
            'version' => '1.5.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpdocumentor/type-resolver',
            'aliases' => array(),
            'reference' => 'a12f7e301eb7258bb68acd89d4aefa05c2906cae',
            'dev_requirement' => true,
        ),
        'phpspec/php-diff' => array(
            'pretty_version' => 'v1.1.3',
            'version' => '1.1.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpspec/php-diff',
            'aliases' => array(),
            'reference' => 'fc1156187f9f6c8395886fe85ed88a0a245d72e9',
            'dev_requirement' => true,
        ),
        'phpspec/prophecy' => array(
            'pretty_version' => '1.14.0',
            'version' => '1.14.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpspec/prophecy',
            'aliases' => array(),
            'reference' => 'd86dfc2e2a3cd366cee475e52c6bb3bbc371aa0e',
            'dev_requirement' => true,
        ),
        'phpunit/php-code-coverage' => array(
            'pretty_version' => '7.0.15',
            'version' => '7.0.15.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/php-code-coverage',
            'aliases' => array(),
            'reference' => '819f92bba8b001d4363065928088de22f25a3a48',
            'dev_requirement' => true,
        ),
        'phpunit/php-file-iterator' => array(
            'pretty_version' => '2.0.5',
            'version' => '2.0.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/php-file-iterator',
            'aliases' => array(),
            'reference' => '42c5ba5220e6904cbfe8b1a1bda7c0cfdc8c12f5',
            'dev_requirement' => true,
        ),
        'phpunit/php-text-template' => array(
            'pretty_version' => '1.2.1',
            'version' => '1.2.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/php-text-template',
            'aliases' => array(),
            'reference' => '31f8b717e51d9a2afca6c9f046f5d69fc27c8686',
            'dev_requirement' => true,
        ),
        'phpunit/php-timer' => array(
            'pretty_version' => '2.1.3',
            'version' => '2.1.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/php-timer',
            'aliases' => array(),
            'reference' => '2454ae1765516d20c4ffe103d85a58a9a3bd5662',
            'dev_requirement' => true,
        ),
        'phpunit/php-token-stream' => array(
            'pretty_version' => '4.0.4',
            'version' => '4.0.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/php-token-stream',
            'aliases' => array(),
            'reference' => 'a853a0e183b9db7eed023d7933a858fa1c8d25a3',
            'dev_requirement' => true,
        ),
        'phpunit/phpunit' => array(
            'pretty_version' => '8.5.21',
            'version' => '8.5.21.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/phpunit',
            'aliases' => array(),
            'reference' => '50a58a60b85947b0bee4c8ecfe0f4bbdcf20e984',
            'dev_requirement' => true,
        ),
        'psr/container' => array(
            'pretty_version' => '2.0.2',
            'version' => '2.0.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/container',
            'aliases' => array(),
            'reference' => 'c71ecc56dfe541dbd90c5360474fbc405f8d5963',
            'dev_requirement' => true,
        ),
        'psr/event-dispatcher' => array(
            'pretty_version' => '1.0.0',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/event-dispatcher',
            'aliases' => array(),
            'reference' => 'dbefd12671e8a14ec7f180cab83036ed26714bb0',
            'dev_requirement' => true,
        ),
        'psr/event-dispatcher-implementation' => array(
            'dev_requirement' => true,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-client' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-client',
            'aliases' => array(),
            'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
            'dev_requirement' => false,
        ),
        'psr/http-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-factory' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-factory',
            'aliases' => array(),
            'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
            'dev_requirement' => false,
        ),
        'psr/http-factory-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-message' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-message',
            'aliases' => array(),
            'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
            'dev_requirement' => false,
        ),
        'psr/http-message-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/log' => array(
            'pretty_version' => '1.1.4',
            'version' => '1.1.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
            'dev_requirement' => false,
        ),
        'psr/log-implementation' => array(
            'dev_requirement' => true,
            'provided' => array(
                0 => '1.0|2.0',
            ),
        ),
        'ralouphie/getallheaders' => array(
            'pretty_version' => '3.0.3',
            'version' => '3.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ralouphie/getallheaders',
            'aliases' => array(),
            'reference' => '120b605dfeb996808c31b6477290a714d356e822',
            'dev_requirement' => false,
        ),
        'sebastian/code-unit-reverse-lookup' => array(
            'pretty_version' => '1.0.2',
            'version' => '1.0.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/code-unit-reverse-lookup',
            'aliases' => array(),
            'reference' => '1de8cd5c010cb153fcd68b8d0f64606f523f7619',
            'dev_requirement' => true,
        ),
        'sebastian/comparator' => array(
            'pretty_version' => '3.0.3',
            'version' => '3.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/comparator',
            'aliases' => array(),
            'reference' => '1071dfcef776a57013124ff35e1fc41ccd294758',
            'dev_requirement' => true,
        ),
        'sebastian/diff' => array(
            'pretty_version' => '3.0.3',
            'version' => '3.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/diff',
            'aliases' => array(),
            'reference' => '14f72dd46eaf2f2293cbe79c93cc0bc43161a211',
            'dev_requirement' => true,
        ),
        'sebastian/environment' => array(
            'pretty_version' => '4.2.4',
            'version' => '4.2.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/environment',
            'aliases' => array(),
            'reference' => 'd47bbbad83711771f167c72d4e3f25f7fcc1f8b0',
            'dev_requirement' => true,
        ),
        'sebastian/exporter' => array(
            'pretty_version' => '3.1.4',
            'version' => '3.1.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/exporter',
            'aliases' => array(),
            'reference' => '0c32ea2e40dbf59de29f3b49bf375176ce7dd8db',
            'dev_requirement' => true,
        ),
        'sebastian/global-state' => array(
            'pretty_version' => '3.0.1',
            'version' => '3.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/global-state',
            'aliases' => array(),
            'reference' => '474fb9edb7ab891665d3bfc6317f42a0a150454b',
            'dev_requirement' => true,
        ),
        'sebastian/object-enumerator' => array(
            'pretty_version' => '3.0.4',
            'version' => '3.0.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/object-enumerator',
            'aliases' => array(),
            'reference' => 'e67f6d32ebd0c749cf9d1dbd9f226c727043cdf2',
            'dev_requirement' => true,
        ),
        'sebastian/object-reflector' => array(
            'pretty_version' => '1.1.2',
            'version' => '1.1.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/object-reflector',
            'aliases' => array(),
            'reference' => '9b8772b9cbd456ab45d4a598d2dd1a1bced6363d',
            'dev_requirement' => true,
        ),
        'sebastian/recursion-context' => array(
            'pretty_version' => '3.0.1',
            'version' => '3.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/recursion-context',
            'aliases' => array(),
            'reference' => '367dcba38d6e1977be014dc4b22f47a484dac7fb',
            'dev_requirement' => true,
        ),
        'sebastian/resource-operations' => array(
            'pretty_version' => '2.0.2',
            'version' => '2.0.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/resource-operations',
            'aliases' => array(),
            'reference' => '31d35ca87926450c44eae7e2611d45a7a65ea8b3',
            'dev_requirement' => true,
        ),
        'sebastian/type' => array(
            'pretty_version' => '1.1.4',
            'version' => '1.1.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/type',
            'aliases' => array(),
            'reference' => '0150cfbc4495ed2df3872fb31b26781e4e077eb4',
            'dev_requirement' => true,
        ),
        'sebastian/version' => array(
            'pretty_version' => '2.0.1',
            'version' => '2.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/version',
            'aliases' => array(),
            'reference' => '99732be0ddb3361e16ad77b68ba41efc8e979019',
            'dev_requirement' => true,
        ),
        'select2/select2' => array(
            'pretty_version' => '4.0.13',
            'version' => '4.0.13.0',
            'type' => 'component',
            'install_path' => __DIR__ . '/../select2/select2',
            'aliases' => array(),
            'reference' => '45f2b83ceed5231afa7b3d5b12b58ad335edd82e',
            'dev_requirement' => false,
        ),
        'swiftmailer/swiftmailer' => array(
            'pretty_version' => 'v6.3.0',
            'version' => '6.3.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../swiftmailer/swiftmailer',
            'aliases' => array(),
            'reference' => '8a5d5072dca8f48460fce2f4131fcc495eec654c',
            'dev_requirement' => false,
        ),
        'symfony/browser-kit' => array(
            'pretty_version' => 'v4.2.4',
            'version' => '4.2.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/browser-kit',
            'aliases' => array(),
            'reference' => '61d85c5af2fc058014c7c89504c3944e73a086f0',
            'dev_requirement' => true,
        ),
        'symfony/console' => array(
            'pretty_version' => 'v5.4.0',
            'version' => '5.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/console',
            'aliases' => array(),
            'reference' => 'ec3661faca1d110d6c307e124b44f99ac54179e3',
            'dev_requirement' => true,
        ),
        'symfony/css-selector' => array(
            'pretty_version' => 'v5.4.0',
            'version' => '5.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/css-selector',
            'aliases' => array(),
            'reference' => '44b933f98bb4b5220d10bed9ce5662f8c2d13dcc',
            'dev_requirement' => true,
        ),
        'symfony/deprecation-contracts' => array(
            'pretty_version' => 'v2.5.0',
            'version' => '2.5.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/deprecation-contracts',
            'aliases' => array(),
            'reference' => '6f981ee24cf69ee7ce9736146d1c57c2780598a8',
            'dev_requirement' => false,
        ),
        'symfony/dom-crawler' => array(
            'pretty_version' => 'v4.4.30',
            'version' => '4.4.30.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/dom-crawler',
            'aliases' => array(),
            'reference' => '4632ae3567746c7e915c33c67a2fb6ab746090c4',
            'dev_requirement' => true,
        ),
        'symfony/event-dispatcher' => array(
            'pretty_version' => 'v5.4.0',
            'version' => '5.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/event-dispatcher',
            'aliases' => array(),
            'reference' => '27d39ae126352b9fa3be5e196ccf4617897be3eb',
            'dev_requirement' => true,
        ),
        'symfony/event-dispatcher-contracts' => array(
            'pretty_version' => 'v2.5.0',
            'version' => '2.5.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/event-dispatcher-contracts',
            'aliases' => array(),
            'reference' => '66bea3b09be61613cd3b4043a65a8ec48cfa6d2a',
            'dev_requirement' => true,
        ),
        'symfony/event-dispatcher-implementation' => array(
            'dev_requirement' => true,
            'provided' => array(
                0 => '2.0',
            ),
        ),
        'symfony/filesystem' => array(
            'pretty_version' => 'v5.4.0',
            'version' => '5.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/filesystem',
            'aliases' => array(),
            'reference' => '731f917dc31edcffec2c6a777f3698c33bea8f01',
            'dev_requirement' => false,
        ),
        'symfony/finder' => array(
            'pretty_version' => 'v5.4.0',
            'version' => '5.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/finder',
            'aliases' => array(),
            'reference' => 'd2f29dac98e96a98be467627bd49c2efb1bc2590',
            'dev_requirement' => true,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(),
            'reference' => '46cd95797e9df938fdd2b03693b5fca5e64b01ce',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-iconv' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-iconv',
            'aliases' => array(),
            'reference' => '63b5bb7db83e5673936d6e3b8b3e022ff6474933',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-grapheme' => array(
            'pretty_version' => 'v1.23.1',
            'version' => '1.23.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-grapheme',
            'aliases' => array(),
            'reference' => '16880ba9c5ebe3642d1995ab866db29270b36535',
            'dev_requirement' => true,
        ),
        'symfony/polyfill-intl-idn' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-idn',
            'aliases' => array(),
            'reference' => '65bd267525e82759e7d8c4e8ceea44f398838e65',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-normalizer' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-normalizer',
            'aliases' => array(),
            'reference' => '8590a5f561694770bdcd3f9b5c69dde6945028e8',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.23.1',
            'version' => '1.23.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'reference' => '9174a3d80210dca8daa7f31fec659150bbeabfc6',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php72' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php72',
            'aliases' => array(),
            'reference' => '9a142215a36a3888e30d0a9eeea9766764e96976',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php73' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php73',
            'aliases' => array(),
            'reference' => 'fba8933c384d6476ab14fb7b8526e5287ca7e010',
            'dev_requirement' => true,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'v1.23.1',
            'version' => '1.23.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(),
            'reference' => '1100343ed1a92e3a38f9ae122fc0eb21602547be',
            'dev_requirement' => false,
        ),
        'symfony/process' => array(
            'pretty_version' => 'v5.4.0',
            'version' => '5.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/process',
            'aliases' => array(),
            'reference' => '5be20b3830f726e019162b26223110c8f47cf274',
            'dev_requirement' => false,
        ),
        'symfony/service-contracts' => array(
            'pretty_version' => 'v1.1.2',
            'version' => '1.1.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/service-contracts',
            'aliases' => array(),
            'reference' => '191afdcb5804db960d26d8566b7e9a2843cab3a0',
            'dev_requirement' => true,
        ),
        'symfony/string' => array(
            'pretty_version' => 'v5.4.0',
            'version' => '5.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/string',
            'aliases' => array(),
            'reference' => '9ffaaba53c61ba75a3c7a3a779051d1e9ec4fd2d',
            'dev_requirement' => true,
        ),
        'symfony/yaml' => array(
            'pretty_version' => 'v5.4.0',
            'version' => '5.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/yaml',
            'aliases' => array(),
            'reference' => '034ccc0994f1ae3f7499fa5b1f2e75d5e7a94efc',
            'dev_requirement' => true,
        ),
        'theseer/tokenizer' => array(
            'pretty_version' => '1.2.1',
            'version' => '1.2.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../theseer/tokenizer',
            'aliases' => array(),
            'reference' => '34a41e998c2183e22995f158c581e7b5e755ab9e',
            'dev_requirement' => true,
        ),
        'webmozart/assert' => array(
            'pretty_version' => '1.10.0',
            'version' => '1.10.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../webmozart/assert',
            'aliases' => array(),
            'reference' => '6964c76c7804814a842473e0c8fd15bab0f18e25',
            'dev_requirement' => true,
        ),
        'yiisoft/yii2' => array(
            'pretty_version' => '2.0.43',
            'version' => '2.0.43.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../yiisoft/yii2',
            'aliases' => array(),
            'reference' => 'f370955faa3067d9b27879aaf14b0978a805cd59',
            'dev_requirement' => false,
        ),
        'yiisoft/yii2-app-advanced' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
        'yiisoft/yii2-bootstrap4' => array(
            'pretty_version' => '2.0.10',
            'version' => '2.0.10.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../yiisoft/yii2-bootstrap4',
            'aliases' => array(),
            'reference' => 'e6d0e58f43d3910129d554ac183aac17f65be639',
            'dev_requirement' => false,
        ),
        'yiisoft/yii2-composer' => array(
            'pretty_version' => '2.0.10',
            'version' => '2.0.10.0',
            'type' => 'composer-plugin',
            'install_path' => __DIR__ . '/../yiisoft/yii2-composer',
            'aliases' => array(),
            'reference' => '94bb3f66e779e2774f8776d6e1bdeab402940510',
            'dev_requirement' => false,
        ),
        'yiisoft/yii2-debug' => array(
            'pretty_version' => '2.1.18',
            'version' => '2.1.18.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../yiisoft/yii2-debug',
            'aliases' => array(),
            'reference' => '45bc5d2ef4e3b0ef6f638190d42f04a77ab1df6c',
            'dev_requirement' => true,
        ),
        'yiisoft/yii2-faker' => array(
            'pretty_version' => '2.0.5',
            'version' => '2.0.5.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../yiisoft/yii2-faker',
            'aliases' => array(),
            'reference' => '8c361657143bfaea58ff7dcc9bf51f1991a46f5d',
            'dev_requirement' => true,
        ),
        'yiisoft/yii2-gii' => array(
            'pretty_version' => '2.2.3',
            'version' => '2.2.3.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../yiisoft/yii2-gii',
            'aliases' => array(),
            'reference' => 'eb14e9cc4c9d80a59e1252e5200600b42aa2bfbf',
            'dev_requirement' => true,
        ),
        'yiisoft/yii2-redis' => array(
            'pretty_version' => '2.0.16',
            'version' => '2.0.16.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../yiisoft/yii2-redis',
            'aliases' => array(),
            'reference' => '1b9efe97d8add594256b51089fbf7a56f31e3c9c',
            'dev_requirement' => false,
        ),
        'yiisoft/yii2-swiftmailer' => array(
            'pretty_version' => '2.1.2',
            'version' => '2.1.2.0',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../yiisoft/yii2-swiftmailer',
            'aliases' => array(),
            'reference' => '09659a55959f9e64b8178d842b64a9ffae42b994',
            'dev_requirement' => false,
        ),
    ),
);
